package com.pagatodo.exceptions;

import java.io.IOException;

public class HttpException extends IOException {

	private static final long serialVersionUID = 7309732956047072127L;
	private int responseCode;

    public HttpException( final int responseCode ) {
        super();
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return this.responseCode;
    }
}