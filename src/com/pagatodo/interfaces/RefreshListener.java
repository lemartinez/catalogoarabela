package com.pagatodo.interfaces;

public interface RefreshListener {

	void doRefresh();

	void refreshFinished();

}
