package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;

public class ListasPojo implements Serializable{

	private static final long serialVersionUID = -5838302980677227962L;
	private ArrayList<Lista> listas;

	public ListasPojo(){
		this.listas=new ArrayList<Lista>();
	}

	public void addList(Lista l){
		listas.add(l);
	}

	public int createList(String nombreLista, Context context){
		int res=0;
		if (nombreLista.equals("")){
			return -2;
		}
		else if(exist(nombreLista)){
			res=-1;
		}
		else{
			Lista l=new Lista(nombreLista);
			this.listas.add(0, l);
		}
		return res;
	}


	public void renameList (int index, String nombre, Context context){

		getLista(index).setNombre(nombre);
	}



	public int dropList(int index, Context context){
		int res=0;
		if(index >= 0 && index<=this.listas.size()){
			this.listas.remove(index);
		}
		else res=-1;
		return res;
	}



	public int getindex(String nom){
		int index=0;
		boolean e=false;
		int cont=0;
		while(e==false&&cont<this.listas.size()){
			if(nom.equals(this.listas.get(cont).getNombre())){
				e=true;
				index=cont;
			}
			else{
				cont++;
			}
		}
		return index;
	}


	public boolean exist(String nom){
		boolean e=false;
		int cont=0;

		while(e==false&&cont<this.listas.size()){
			if(nom.equals(this.listas.get(cont).getNombre())){
				e=true;
			}
			else{
				cont++;
			}
		}
		return e;
	}


	public ArrayList<Lista> getListas(){
		return this.listas;
	}

	public void  setListas(ArrayList<Lista> listas){
		this.listas = listas;
	}

	public Lista getLista(int index){
		Lista l=null;
		if(index>=0 && index<this.listas.size())
			l=this.listas.get(index);
		return l;
	}
	
	public void remplaceList(Lista l){
		this.listas.remove(getindex(l.getNombre()));
		this.listas.add(l);
	}

}
