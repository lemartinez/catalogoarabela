package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Category implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2469914735582852137L;
	
	public String id;
	public String name;
	public String image;
	public int products_total;
	
	@SerializedName("final")
	public boolean final_;

}