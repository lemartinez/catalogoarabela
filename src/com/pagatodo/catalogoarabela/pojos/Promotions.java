package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Promotions implements Serializable{


	private static final long serialVersionUID = -7860787697333130867L;
	
	@SerializedName("top_banners")
	public List<Banner> topBanners;
	@SerializedName("bottom_banners")
	public List<Banner> bottomBanners;
	public Error error;
}

