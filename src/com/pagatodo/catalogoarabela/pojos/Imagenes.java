package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;

public class Imagenes implements Serializable{
	private static final long serialVersionUID = -7838632796905664463L;
	public String id;
	public String url;
}