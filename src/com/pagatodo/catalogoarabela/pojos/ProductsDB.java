package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;
import java.util.List;

public class ProductsDB implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8052942507437056303L;
	public List<ProductDB> products;
	public List<ProductCategory> productsCategories;
}
