package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;

public class Error implements Serializable{

	private static final long serialVersionUID = 6939336703156930929L;
	
	
	public String message;
	public String title;
	
}
