package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;
import java.util.List;

public class Product implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5814617278709338677L;
	public String sku;
	public String name;
	public String price;
	public String price_with_discount;
	public String url;

	public List<Imagenes> images;
	public List<String> colors;
	public List<String> sizes;

	public boolean availability;
	public String thumbnail;
	public String image_main;
	public String description;
	
	/*Valores para el carrito de compras que se leen desde la BD*/
	public int cantidad = 0 ;
	public long toc = -1; //time of creation
	public long tom = -1; //time of modification
	
	public Error error;
	

}

