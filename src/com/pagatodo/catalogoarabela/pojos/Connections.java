package com.pagatodo.catalogoarabela.pojos;

public class Connections {
	
	public String URL_VERIF;
	public String URL_SERVER;
	public String URL_CATEGORIES;
	public String URL_PRODUCTS;
	public String URL_SHORTENER;
	
	public boolean equals(Connections c) {
		if(!c.URL_CATEGORIES.equals(this.URL_CATEGORIES))
			return false;
		if(!c.URL_VERIF.equals(this.URL_VERIF))
			return false;
		if(!c.URL_SERVER.equals(this.URL_SERVER))
			return false;
		if(!c.URL_PRODUCTS.equals(this.URL_PRODUCTS))
			return false;
		if(!c.URL_SHORTENER.equals(this.URL_SHORTENER))
			return false;
		return true;
	}
}
