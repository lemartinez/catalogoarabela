package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.text.format.DateFormat;

public class Lista implements Serializable{

	private static final long serialVersionUID = 2927713888420882864L;
	
	private String nombre;
	private ArrayList<Product> productos;
	private String dateOfCreation;
	
	public Lista(String nombre){
		this.nombre=new String(nombre);
		this.productos=new ArrayList<Product>();
		this.dateOfCreation = DateFormat.format("dd-MM-yyyy", new Date()).toString();
	}
	
	public String getNombre (){
		return this.nombre;
	}

	public void setNombre (String nombre){
		this.nombre= nombre;
	}
	
	public String getDateOfCreation(){
		return this.dateOfCreation;
	}
	
	public Double getTotalAmount(){
		Double total=0.0;
		for(Product p : productos){
			if(p.price_with_discount!=null&&!p.price_with_discount.equals(""))
				total+=Double.parseDouble(p.price_with_discount)*p.cantidad;
			else
				if(p.price!=null&&!p.price.equals(""))
					total+=Double.parseDouble(p.price)*p.cantidad;
		}
		return total;
	}
	
	
	public ArrayList<Product> getProductos() {
		return this.productos;
	}
	
	public int getNumberProducts(){
		int totalProducts=0;
		for(Product p: productos){
			totalProducts+=p.cantidad;
		}
		return totalProducts;
	}

	public void setProductos(ArrayList<Product> productos) {
		this.productos = productos;
	}
	
	public boolean exist(String nom){
		boolean e=false;
		int cont=0;

		while(e==false&&cont<this.productos.size()){
			if(nom.equals(this.productos.get(cont).name)){
				e=true;
			}
			else{
				cont++;
			}
		}
		return e;
	}
	
	public int getIndexOfProduct(String nom){
		int cont=0;

		while(cont<this.productos.size()){
			if(nom.equals(this.productos.get(cont).name))
				break;
			else
				cont++;
		}
		return cont;
	}

	public boolean addProduct (Product product, Context context){
		boolean ans = false;
		
		if ( product != null ){
			if(!exist(product.name)){
				product.cantidad=1;
				getProductos().add(product);
			}else{
				int index = getIndexOfProduct(product.name);
				productos.get(index).cantidad++;
			}
		}
		return ans;
	}
	
	public boolean setCantidad (Product product,int cantidad, Context context){
		boolean ans = false;
		if ( product != null ){
			productos.get(productos.indexOf(product)).cantidad=cantidad;
		}
		return ans;
	}
	
	
	public boolean removeProduct(int id, Context context){
		getProductos().remove(id);
		return true;
	}

}
