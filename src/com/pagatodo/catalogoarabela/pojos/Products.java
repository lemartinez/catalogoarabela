package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;
import java.util.List;

public class Products implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2100826286629978480L;
	public int total;
	public int page;
	public int total_pages;
	public List<Product> products;
	public Error error;

}
