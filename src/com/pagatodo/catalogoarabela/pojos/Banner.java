package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;

public class Banner 	implements Serializable{

	private static final long serialVersionUID = -6642893434491995525L;
	
	public String image;
	public String name;
	public String type;
	public int id;
	public String value;
}