package com.pagatodo.catalogoarabela.pojos;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Category2 implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8891989503451444022L;
	public String id;
	public String name;
	public String id_parent;
	public int total;
	public String image;
	public int products_total;
	
	@SerializedName("final")
	public boolean final_;
	
	public List <Category2> categories;

}
