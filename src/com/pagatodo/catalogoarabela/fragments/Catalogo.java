package com.pagatodo.catalogoarabela.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;
import com.pagatodo.catalogoarabela.Arabela;
import com.pagatodo.catalogoarabela.R;

public class Catalogo extends SherlockFragment{
	
	int mNum;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mNum = getArguments() != null ? getArguments().getInt("num") : 1;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.catalogo, container, false);
		((Arabela)getActivity()).hideBarSearch();
//		TextView tv=(TextView) v.findViewById(R.id.tv_number_fragment);
//		tv.setText("Cat�logo #" + mNum);
//		Button button = (Button)v.findViewById(R.id.new_fragment);
//        button.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                addFragmentToStack();
//            }
//        });
		return v;
	}

	void addFragmentToStack() {
		Bundle bundle = new Bundle();
		bundle.putInt("num", mNum+1);
		Fragment fragment = new Catalogo();
		fragment.setArguments(bundle);
        ((Arabela)getActivity()).startNextFragment(fragment);
    } 
}