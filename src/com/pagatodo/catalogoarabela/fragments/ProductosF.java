package com.pagatodo.catalogoarabela.fragments;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.pagatodo.catalogoarabela.AppArabela;
import com.pagatodo.catalogoarabela.Arabela;
import com.pagatodo.catalogoarabela.R;
import com.pagatodo.catalogoarabela.db.MyDB;
import com.pagatodo.catalogoarabela.pojos.Product;
import com.pagatodo.catalogoarabela.pojos.Products;
import com.pagatodo.catalogoarabela.recursos.Recursos.AnsType;
import com.pagatodo.exceptions.HttpException;
import com.pagatodo.exceptions.OfflineException;
import com.pagatodo.interfaces.GetCategory;
import com.pagatodo.interfaces.RefreshListener;
import com.pagatodo.interfaces.Update;
import com.pagatodo.net.Net;
import com.pagatodo.ui.UI;
import com.pagatodo.utils.PullToRefreshComponent;
import com.pagatodo.utils.Sonido;
import com.pagatodo.utils.Utils;


public class ProductosF extends ListFragment implements Update, GetCategory {

	protected ProductosAdapter t_adapter=null;
	private String id = null;
	protected DisplayImageOptions options;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	private ProgressDialog m_ProgressDialog = null;
	protected Activity context;
	protected View view = null;
	private String search;
	private boolean resume=false;
	private String name = null;
	protected Integer page;
	private int total;
	protected int totalPages;
	protected RelativeLayout rlFooterProducts;
	protected ProgressBar pbFooter;
	protected boolean refresh=false;
	protected PullToRefreshComponent pullToRefresh;
	public enum Type {NORMAL, LISTA, PROMOCION}
	private Type type = Type.NORMAL;
	private Sonido sonido;
	private final int  audio = R.raw.tab; 
	private MyDB dba;
	protected Boolean webServices = false;
	private Products products = null;

	public void setType(Type type ){
		this.type = type;
	}

	public void setName(String name){
		this.name=new String(name);
	}


	public ProductosF(String id_category, String search) {
		this.search = search;
		this.id = id_category;
	}


	public ProductosF(String id_category, int total) {
		this.id = id_category;
		this.total = total;
	}

	public ProductosF(String id) {
		this.id = id;
	}


	public ProductosF() {
		this(null);
	}

	public void setId(String id){
		this.id=id;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		this.context = getActivity();
		this.sonido = new Sonido(this.context);
		sonido.initSonido(this.audio);
		this.page=1;
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.stub_)
		.showImageForEmptyUrl(R.drawable.stub_2)
		.cacheInMemory()
		.cacheOnDisc()
		.build();

		imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		this.dba = new MyDB(this.context.getApplicationContext());

		if(savedInstanceState!=null){

			if (savedInstanceState.containsKey("id"))
				id = savedInstanceState.getString("id");

			if (savedInstanceState.containsKey("search"))
				search = savedInstanceState.getString("search");

			if (savedInstanceState.containsKey("name"))
				name = savedInstanceState.getString("name");

			type = (Type) savedInstanceState.getSerializable("type");
			total = savedInstanceState.getInt("total");

		}

	}


	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (id != null ) outState.putSerializable("id", id);
		if (search != null )  outState.putSerializable("search", search);
		if (name != null )  outState.putSerializable("name", name);

		outState.putSerializable("total", total);
		outState.putSerializable("type", type);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		this.view = inflater.inflate(R.layout.productos, container, false);
		this.view.setVisibility(View.INVISIBLE);
		this.rlFooterProducts = (RelativeLayout) view.findViewById(R.id.footer);
		this.pbFooter = (ProgressBar) view.findViewById(R.id.pull_to_refresh_progress);
		return this.view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (getListAdapter() == null){
			this.t_adapter = new ProductosAdapter(this.context, R.layout.product_item, new ArrayList<Product>());
			setListAdapter(this.t_adapter);
		}
		this.pullToRefresh = new PullToRefreshComponent(this.getListView(), new Handler());
		this.pullToRefresh.setOnPullUpRefreshAction(new RefreshListener() {
			@Override
			public void refreshFinished() {
				ProductosF.this.context.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(ProductosF.this.totalPages>1&&ProductosF.this.page<ProductosF.this.totalPages&&refresh==false){
							ProductosF.this.refresh=true;
							ProductosF.this.page++;
							showFooter();
							showProgressFooter();
							new Thread(null, ProductosF.this.runGetProductsDB, "runGetProducts").start();
						}
					}
				});
			}
			@Override
			public void doRefresh() {}
		});
		updateData();
	}

	@Override
	public void onResume() {
		super.onResume();
		this.resume=true;
	}

	@Override
	public void onPause() {
		this.resume=false;
		super.onPause();
	}

	@Override
	public void onDestroy() {
		
		clear();
		imageLoader.stop();
		if(this.name!=null){
			((Arabela)this.context).removeDir("/"+this.name);
		}
		
		super.onDestroy();
	}



	@Override
	public void updateData(){
		if(ProductosF.this.products!=null&&ProductosF.this.webServices==false){
			ProductosF.this.context.runOnUiThread(new ResGetProducts(products,AnsType.OK));
		}else{
			new Thread(null, this.runGetProductsDB, "runGetProducts").start();
			this.m_ProgressDialog = ProgressDialog.show(this.context, getString(R.string.espere), getString(R.string.obteniendo), true,true,
					new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					if(ProductosF.this.page==1){
						( (Arabela) ProductosF.this.context).removeFragment();
					}
				}
			});
		}
	}

	class ProductosAdapter extends ArrayAdapter<Product> {

		private int textViewResourceId; 
		public ProductosAdapter(Context context, int textViewResourceId, ArrayList<Product> products ) {
			super(context , textViewResourceId, products);
			this.textViewResourceId = textViewResourceId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			View v = convertView;
			if (v == null || !(v.getTag() instanceof ViewHolder)) {
				LayoutInflater vi = LayoutInflater.from(ProductosF.this.context);
				v = vi.inflate(this.textViewResourceId, null);
				holder = new ViewHolder();
				holder.name = (TextView) v.findViewById(R.id.product_title);
				holder.price = (TextView)  v.findViewById(R.id.product_normal_price);
				holder.price_with_discount = (TextView)  v.findViewById(R.id.product_discount_price);
				holder.image=(ImageView)v.findViewById(R.id.product_image);
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}



			Product o = super.getItem(position);
			if (o != null){
				String  name = o.name!=null?o.name.trim():""; //Html.fromHtml( 
				holder.name.setText( Html.fromHtml(name ) );  //Formateando el texto en html
				holder.price.setText(Utils.stringtoCurrency(o.price));
				imageLoader.displayImage(o.thumbnail, holder.image, options,new MyImageLoadingListener(holder.image));

				if (o.price_with_discount != null){
					holder.price_with_discount.setText(Utils.stringtoCurrency(o.price_with_discount));
					holder.price_with_discount.setVisibility(View.VISIBLE);
					holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
				}
				else {
					holder.price.setPaintFlags( holder.price.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
					holder.price_with_discount.setVisibility(View.INVISIBLE);
				}

			}
			return v;
		}
	}


	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		if (position < 0)
			return;
		Product o = this.t_adapter.getItem(position);
		if (o != null){
			sonido.play(false);
			Fragment fragmnet = new ProductoF(o,webServices);
			((Arabela)this.context).addDir(o.name);
			clear();
			this.page=1;
			hideFooter();
			((Arabela) this.context).startNextFragment(fragmnet);

		}
	}

	private void clear(){
		this.t_adapter.clear();
		this.t_adapter.notifyDataSetChanged();
	}

	public void showFooter(){
		this.rlFooterProducts.setVisibility(View.VISIBLE);
	}

	public void hideFooter(){
		this.rlFooterProducts.setVisibility(View.GONE);
	}
	public void showProgressFooter(){
		this.pbFooter.setVisibility(View.VISIBLE);
	}

	public void hideprogressFooter(){
		this.pbFooter.setVisibility(View.INVISIBLE);
	}

	/*
	 *Runnables para ejecutar los webServices getCategories y getProducts 
	 *
	 */
	protected Runnable runGetProductsDB = new Runnable(){
		@Override
		public void run() {
			AnsType ansType = AnsType.UNKNOW;
			boolean reintent=false;
			try {
				String url;
				if (ProductosF.this.type == Type.PROMOCION){
					url = String.format(AppArabela.getInstance().getPrefs().loadData("URL_SERVER") + "productsPromo/%s/",  URLEncoder.encode(id,  "UTF-8")) ;
					url = Net.addParameter(url, "page", page.toString());
					products = (Products) Net.sendDataAndGetObjectCache(url, Products.class, null);
					ProductosF.this.webServices = true;
				}else{
					ProductosF.this.dba.open();
					products = ProductosF.this.dba.getProductos(ProductosF.this.id);
					ProductosF.this.dba.close();
				}
				ansType = AnsType.OK;
			} catch (HttpException e) {					
				reintent=Utils.getUrls();
				ansType = AnsType.NETWORK_PROBLEM;
			} catch (IOException e) {
				ansType = AnsType.NETWORK_PROBLEM;
				e.printStackTrace();
			} catch (OfflineException e) {
				ansType = AnsType.OFFLINE;
				e.printStackTrace();
			} catch (Exception e) {
				ansType = AnsType.UNKNOW;
				e.printStackTrace();
			}
			if(reintent==true)
				new Thread(null, ProductosF.this.runGetProductsDB, "runGetProducts").start();
			else{
				if(ProductosF.this.search==null&&(ProductosF.this.type == Type.PROMOCION||products.total==ProductosF.this.total)&&products.total>0)
					ProductosF.this.context.runOnUiThread(new ResGetProducts(products,ansType));
				else
					new Thread(null, ProductosF.this.runGetProducts, "runGetProducts").start();
			}
		}
	};
	protected Runnable runGetProducts = new Runnable(){
		@Override
		public void run() {
			ProductosF.this.webServices = true;
			AnsType ansType = AnsType.UNKNOW;
			boolean reintent=false;
			try {
				String url;
				url = AppArabela.getInstance().getPrefs().loadData("URL_SERVER") + "products/" ;
				url = Net.addParameter(url, "cat", id);
				url = Net.addParameter(url, "name", search);
				url = Net.addParameter(url, "page", page.toString());
				products = (Products) Net.sendDataAndGetObjectCache(url, Products.class, null);
				ansType = AnsType.OK;
			} catch (HttpException e) {					
				reintent=Utils.getUrls();
				ansType = AnsType.NETWORK_PROBLEM;
			} catch (IOException e) {
				ansType = AnsType.NETWORK_PROBLEM;
				e.printStackTrace();
			} catch (OfflineException e) {
				ansType = AnsType.OFFLINE;
				e.printStackTrace();
			} catch (Exception e) {
				ansType = AnsType.UNKNOW;
				e.printStackTrace();
			}
			if(reintent==true)
				new Thread(null, ProductosF.this.runGetProducts, "runGetProducts").start();
			else
				ProductosF.this.context.runOnUiThread(new ResGetProducts(products,ansType));
		}
	};

	class  ResGetProducts implements Runnable {

		List <Product> productos;
		com.pagatodo.catalogoarabela.pojos.Error error;
		AnsType ansType;
		public ResGetProducts (Products products,AnsType ansType){
			if ( products != null){
				this.productos=null;
				ProductosF.this.page = products.page;
				ProductosF.this.totalPages = products.total_pages;
				this.productos = products.products;
				this.error = products.error;
			} 
			this.ansType = ansType;
		}
		@Override
		public void run() {

			hideprogressFooter();
			switch (this.ansType) {
			case OK:
				if(this.productos != null && this.productos.size() > 0 && this.error == null){	
					for( Product product : this.productos){
						ProductosF.this.t_adapter.add(product);
					}
					hideFooter();
					ProductosF.this.view.setVisibility(View.VISIBLE);
				}
				else {

					if(ProductosF.this.page==1){	
						((Arabela) context).removeFragment();	
					}


					if (error == null){
						error = new com.pagatodo.catalogoarabela.pojos.Error();

						error.title = getString(R.string.lo_sentimos);
						error.message = getString(R.string.no_product);
					}

					UI.showAlertDialog( error.title, error.message, getString(R.string.ok), context, null).show();

				}
				InputMethodManager imm = (InputMethodManager)ProductosF.this.context.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(ProductosF.this.view.getWindowToken(), 0);
				ProductosF.this.t_adapter.notifyDataSetChanged();
				ProductosF.this.refresh=false;
				break;
			case OFFLINE:
				if(ProductosF.this.resume==true){
					if(ProductosF.this.page==1){
						( (Arabela) ProductosF.this.context).removeFragment();
					}
					else
						hideFooter();
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_sin_internet), getString(R.string.ok), ProductosF.this.context, null).create().show();
				}
				break;

			case NETWORK_PROBLEM:
				if(ProductosF.this.resume==true){
					if(ProductosF.this.page==1){
						( (Arabela) ProductosF.this.context).removeFragment();
					}
					else
						hideFooter();
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_error), getString(R.string.ok), ProductosF.this.context, null).create().show();
				}
				break;
			case UNKNOW:
			case DATA_PROBLEM:
			default:
				if(ProductosF.this.resume==true){
					if(ProductosF.this.page==1){
						( (Arabela) ProductosF.this.context).removeFragment();
					}
					else
						hideFooter();
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_datos_corruptos), getString(R.string.ok), ProductosF.this.context, null).create().show();
				}
				break;
			}
			ProductosF.this.m_ProgressDialog.dismiss();
		}
	}

	@Override
	public String getCategory() {
		return  this.id;
	}

	private static class ViewHolder {
		TextView name;
		TextView price;
		TextView price_with_discount;
		ImageView image;
	}

	class MyImageLoadingListener implements ImageLoadingListener {
		private ImageView image;
		public MyImageLoadingListener (ImageView image ){
			this.image = image;
		}
		@Override
		public void onLoadingCancelled() {}
		@Override
		public void onLoadingComplete() {}
		@Override
		public void onLoadingFailed(FailReason arg0) {
			this.image.setImageResource(R.drawable.stub_2);
		}
		@Override
		public void onLoadingStarted() {}

	};
}