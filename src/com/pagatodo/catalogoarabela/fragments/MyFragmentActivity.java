package com.pagatodo.catalogoarabela.fragments;

import static com.pagatodo.catalogoarabela.recursos.Recursos.SECRET_TOKEN;
import static com.pagatodo.catalogoarabela.recursos.Recursos.TOKEN;

import java.util.HashMap;
import java.util.Stack;
import java.util.UUID;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.TabHost;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.pagatodo.catalogoarabela.AppArabela;
import com.pagatodo.catalogoarabela.Arabela.TabManager;
import com.pagatodo.catalogoarabela.recursos.Recursos;
import com.pagatodo.persistencia.Preferencias;
import com.pagatodo.ui.AlertDialogShareTwitter;
import com.pagatodo.ui.DialogTwitterAuth;

public class MyFragmentActivity extends SherlockFragmentActivity{
	
	private int fagmentId = -1;
	
	private String dir=null;
	
	private String messageToShare="www.pagatodo.com";
	private DialogTwitterAuth mDialogTwitterAuth;
	private Preferencias prefs;
	
	protected static HashMap<String, Stack<String>> backStacks;
	protected TabHost mTabHost;
	protected TabManager mTabManager;
	protected enum StackType{ENVIADOS, NO_ENVIADOS, RECIBIDOS,CATALOGO}
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		this.prefs= AppArabela.getInstance().getPrefs();
	}
	
	public void removeFragmentFromStack(Stack<String> backStack)
	{
		String tag = backStack.pop();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
		ft.remove(fragment);
		ft.commitAllowingStateLoss();
	}
	public void showCorrectFragment(Stack<String> backStack, int containerId)
	{
		String tag = backStack.peek();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
		ft.replace(containerId, fragment);
		ft.commitAllowingStateLoss();
	}
	
	public void startNextFragmentToStack(Fragment fragment,Stack<String> backStack, int containerId){
		String tag = UUID.randomUUID().toString();
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(containerId, fragment,tag);
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commitAllowingStateLoss();
		backStack.push(tag);
		return;
	}
	
	public void startNextFragmentToTab(Fragment fragment){
		Stack<String> backStack = backStacks.get(mTabHost.getCurrentTabTag());
		startNextFragmentToStack(fragment,backStack,mTabManager.getContainerId());
		return;
	}

	public void startNextFragment(Fragment fragment){
		Stack<String> backStack = backStacks.get(StackType.CATALOGO.toString());
		startNextFragmentToStack(fragment,backStack,fagmentId);
		return;
	}
	
	/*
	 * GETTERS y SETTERS
	 * 
	 * */
	public void setDir(String dir){
		this.dir=new String(dir);
	}
	public String getDir(){
		return this.dir;
	}
	public int getFagmentId() {
		return this.fagmentId;
	}

	public void setFragmentId(int fragmenfagmentId) {
		this.fagmentId = fragmenfagmentId;
	}
	
	/*
	 * Rastreo de P�ginas y Eventos
	 * 
	 * */
	public void addDir(String dir){
		this.dir=new String(this.dir.concat("/"+dir));
		trackPages();
	}
	public void removeDir(String dir){
		try {

			if (this.dir.indexOf(dir)>= 0)
				this.dir=this.dir.substring(0,this.dir.length()-dir.length());
			trackPages();

		} catch (Exception e) {}
	}

	public void trackPages(){
		if(!this.dir.equals("")){
			Intent intent = new Intent(Recursos.TRACK_PAGE);
			intent.putExtra("DIR",this.dir);
			sendBroadcast(intent);
		}
	}

	public void trackEvents( String category , String action , String label, int value){
		Intent intent = new Intent(Recursos.TRACK_EVENT);
		intent.putExtra("CATEGORY", category);
		intent.putExtra("ACTION", action);
		intent.putExtra("LABEL", label);
		intent.putExtra("VALUE", value);
		sendBroadcast(intent);
	}
	
	public void hideKeyboard (){
		sendBroadcast(new Intent(Recursos.HIDE_KEYBOARD));
	}
	
	/*
	 * Twiter 
	 * 
	 * */

	public void setMessageToShare(String message){
		this.messageToShare=message;
	}

	public void sharetw(){
		if(prefs.loadData(TOKEN)!=null&&prefs.loadData(SECRET_TOKEN)!=null)
			Tweet();
		else{
			mDialogTwitterAuth = new DialogTwitterAuth(this);
			mDialogTwitterAuth.show();
		}
		return;
	}
	public void Tweet(){
		new AlertDialogShareTwitter(messageToShare, this).buildShow();
	}

}
