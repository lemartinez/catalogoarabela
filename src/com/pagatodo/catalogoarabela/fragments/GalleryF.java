package com.pagatodo.catalogoarabela.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.pagatodo.catalogoarabela.R;
import com.pagatodo.catalogoarabela.pojos.Imagenes;


@SuppressWarnings("deprecation")
public class GalleryF extends Fragment {

	private Activity context;
	private View view = null;
	private ArrayList<Imagenes> imagenes;
	private ImageView image;
	private DisplayImageOptions options;
	private ImageLoader imageLoader = ImageLoader.getInstance();

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		this.context = getActivity();

		if (getArguments() != null){
			this.imagenes = (ArrayList<Imagenes>) getArguments().getSerializable("IMAGENES");
		}

		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.stub)
		.showImageForEmptyUrl(R.drawable.stub_2)
		.cacheInMemory()
		.cacheOnDisc()
		.build();
		imageLoader.init(ImageLoaderConfiguration.createDefault(context));
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (this.imagenes != null ) outState.putSerializable("IMAGENES", this.imagenes);


	}


	@SuppressWarnings({ "cast" })
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		this.view = (View) inflater.inflate(R.layout.gallery, container, false);
		Gallery gallery = (Gallery) this.view.findViewById(R.id.gallery);
		this.image = (ImageView) this.view.findViewById(R.id.image);

		gallery.setAdapter(new ImageAdapter(this.context));
		gallery.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				imageLoader.displayImage(GalleryF.this.imagenes.get(arg2).url, image, options,new ImageLoadingListener() {
					@Override
					public void onLoadingCancelled() {}
					@Override
					public void onLoadingComplete() {
						image.setScaleType(ScaleType.FIT_CENTER);
					}
					@Override
					public void onLoadingFailed(FailReason arg0) {
						image.setImageResource(R.drawable.stub_2);
					}
					@Override
					public void onLoadingStarted() {
						image.setScaleType(ScaleType.FIT_XY);
					}

				});				
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
		return this.view;
	}

	@Override
	public void onDestroy() {
		imageLoader.stop();
		((MyFragmentActivity)this.context).removeDir("/Galeria");
		super.onDestroy();
	}
	class ImageAdapter extends BaseAdapter{
		int mGalleryItemBackground;
		private Context mContext;

		public ImageAdapter(Context c) {
			this.mContext = c;
			TypedArray attr = this.mContext.obtainStyledAttributes(R.styleable.GalleryTheme);
			this.mGalleryItemBackground = attr.getResourceId( R.styleable.GalleryTheme_android_galleryItemBackground, 0);
			attr.recycle();
		}
		@Override
		public int getCount() {
			return GalleryF.this.imagenes.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			final ImageView imageView = new ImageView(this.mContext);
			imageView.setLayoutParams(new Gallery.LayoutParams(150,100));
			imageView.setBackgroundResource( R.drawable.rounded_rectangle);

			imageLoader.displayImage(GalleryF.this.imagenes.get(position).url, imageView, options,new ImageLoadingListener() {
				@Override
				public void onLoadingCancelled() {}
				@Override
				public void onLoadingComplete() {
					imageView.setScaleType(ScaleType.FIT_CENTER);
				}
				@Override
				public void onLoadingFailed(FailReason arg0) {
					image.setImageResource(R.drawable.stub_2);
				}
				@Override
				public void onLoadingStarted() {
					imageView.setScaleType(ScaleType.CENTER);
				}
			});
			return imageView;
		}
	}
}
