package com.pagatodo.catalogoarabela.fragments;

import static com.pagatodo.catalogoarabela.recursos.Recursos.UPDATE_PRODUCTS;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.text.Html;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.pagatodo.catalogoarabela.AppArabela;
import com.pagatodo.catalogoarabela.Arabela;
import com.pagatodo.catalogoarabela.R;
import com.pagatodo.catalogoarabela.db.MyDB;
import com.pagatodo.catalogoarabela.fragments.MyFragmentActivity.StackType;
import com.pagatodo.catalogoarabela.pojos.Lista;
import com.pagatodo.catalogoarabela.pojos.ListasPojo;
import com.pagatodo.catalogoarabela.pojos.Product;
import com.pagatodo.catalogoarabela.pojos.Products;
import com.pagatodo.catalogoarabela.recursos.Recursos.AnsType;
import com.pagatodo.exceptions.HttpException;
import com.pagatodo.exceptions.OfflineException;
import com.pagatodo.interfaces.GetCategory;
import com.pagatodo.interfaces.RefreshListener;
import com.pagatodo.interfaces.Update;
import com.pagatodo.net.Net;
import com.pagatodo.persistencia.FileSdStorage;
import com.pagatodo.ui.UI;
import com.pagatodo.utils.PullToRefreshComponent;
import com.pagatodo.utils.Sonido;
import com.pagatodo.utils.Utils;


public class ProductosListaF extends ListFragment implements Update, GetCategory{

	final static CharSequence[] items ={ "1",  "2",  "3",  "4",  "5",  "6",  "7",  "8",  "9", "10",
		"11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
		"21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
		"31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
		"41", "42", "43", "44", "45", "46", "47", "48", "49", "50"};

	private String id = null;
	protected DisplayImageOptions options;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	protected View view = null;
	private String search;
	private boolean resume=false;
	private String name = null;
	protected Integer page;
	private int total;
	protected int totalPages;
	protected ProgressBar pbFooter;
	protected boolean refresh=false;
	protected PullToRefreshComponent pullToRefresh;
	public enum Type {NORMAL, LISTA, PROMOCION}
	private Type type = Type.NORMAL;
	private Sonido sonido;
	private final int  audio = R.raw.tab; 
	private MyDB dba;
	protected Boolean webServices = false;
	private Products products = null;

	protected PedidoAdapter t_adapter=null;
	private Lista lista;
	private Activity context;
	private ImageButton btnListasAgregar;
	private ImageButton btnListasEnviar;
	private ImageButton btnRecibirPedido;
	protected LinearLayout rlFooterProducts;
	private TextView tvNameLista;
	private TextView tvTotalAmount;

	public void setType(Type type ){
		this.type = type;
	}

	public void setName(String name){
		this.name=new String(name);
	}


	public ProductosListaF(String id_category, String search) {
		this.search = search;
		this.id = id_category;
	}


	public ProductosListaF(String id_category, int total) {
		this.id = id_category;
		this.total = total;
	}

	public ProductosListaF(String id) {
		this.id = id;
	}


	public ProductosListaF() {
		this(null);
	}

	public void setId(String id){
		this.id=id;
	}

	public void setLista (Lista lista ){
		this.lista = lista;
	}

	public Lista getLista (){
		return lista;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		this.context = getActivity();
		this.sonido = new Sonido(this.context);
		sonido.initSonido(this.audio);
		this.page=1;
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.stub_)
		.showImageForEmptyUrl(R.drawable.stub_2)
		.cacheInMemory()
		.cacheOnDisc()
		.build();

		imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		this.dba = new MyDB(this.context.getApplicationContext());

		webServices = true;

		if(savedInstanceState!=null){

			if (savedInstanceState.containsKey("id"))
				id = savedInstanceState.getString("id");

			if (savedInstanceState.containsKey("search"))
				search = savedInstanceState.getString("search");

			if (savedInstanceState.containsKey("name"))
				name = savedInstanceState.getString("name");

			if (savedInstanceState.containsKey("lista"))
				lista = (Lista) savedInstanceState.getSerializable("lista");

			type = (Type) savedInstanceState.getSerializable("type");
			total = savedInstanceState.getInt("total");

		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (id != null ) outState.putSerializable("id", id);
		if (search != null )  outState.putSerializable("search", search);
		if (name != null )  outState.putSerializable("name", name);

		outState.putSerializable("total", total);
		outState.putSerializable("type", type);
		if (lista != null ) outState.putSerializable("lista", lista);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {		
		this.view = inflater.inflate(R.layout.productos_en_pedido, container, false);
		this.view.setVisibility(View.INVISIBLE);
		this.context = getActivity();
		rlFooterProducts = (LinearLayout) view.findViewById(R.id.footer);
		pbFooter = (ProgressBar) view.findViewById(R.id.pull_to_refresh_progress);
		btnListasAgregar = (ImageButton) view.findViewById(R.id.btn_agregar_producto);
		btnListasEnviar = (ImageButton) view.findViewById(R.id.btn_enviar_pedido);
		btnRecibirPedido = (ImageButton) view.findViewById(R.id.btn_pedido_recibido);
		tvNameLista = (TextView) view.findViewById(R.id.pedido_title);
		tvTotalAmount = (TextView) view.findViewById(R.id.pedido_amount);
		tvNameLista.setText(getLista().getNombre());
		tvTotalAmount.setText(getString(R.string.pedido_total)+" "+Utils.stringtoCurrency(getLista().getTotalAmount().toString()));
		btnListasAgregar.setOnClickListener(agregarProducto);
		btnListasEnviar.setOnClickListener(enviarLista);
		btnRecibirPedido.setOnClickListener(recibirLista);
		if(((Arabela)context).mTabManager.mTabHost.getCurrentTabTag().equals(StackType.ENVIADOS.toString())){
			btnListasAgregar.setVisibility(View.GONE);
			btnListasEnviar.setVisibility(View.GONE);
			btnRecibirPedido.setVisibility(View.VISIBLE);
		}else{
			if(((Arabela)context).mTabManager.mTabHost.getCurrentTabTag().equals(StackType.RECIBIDOS.toString())){
				rlFooterProducts.setVisibility(View.GONE);
			}
			else{
				btnListasAgregar.setVisibility(View.VISIBLE);
				btnListasEnviar.setVisibility(View.VISIBLE);
				btnRecibirPedido.setVisibility(View.GONE);
			}
		}
		return this.view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		registerForContextMenu(getListView());
		if (getListAdapter() == null){
			this.t_adapter = new PedidoAdapter(this.context, R.layout.product_pedido_item, new ArrayList<Product>());
			setListAdapter(this.t_adapter);
		}
		this.pullToRefresh = new PullToRefreshComponent(this.getListView(), new Handler());
		this.pullToRefresh.setOnPullUpRefreshAction(new RefreshListener() {
			@Override
			public void refreshFinished() {
				ProductosListaF.this.context.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(ProductosListaF.this.totalPages>1&&ProductosListaF.this.page<ProductosListaF.this.totalPages&&refresh==false){
							ProductosListaF.this.refresh=true;
							ProductosListaF.this.page++;
							showProgressFooter();
							new Thread(null, ProductosListaF.this.runGetProductsDB, "runGetProducts").start();
						}
					}
				});
			}
			@Override
			public void doRefresh() {}
		});
		updateData();
		IntentFilter updatedata = new IntentFilter(UPDATE_PRODUCTS);
		this.context.registerReceiver(this.update, updatedata);
	}

	@Override
	public void onResume() {
		super.onResume();
		this.resume=true;
	}

	@Override
	public void onPause() {
		this.resume=false;
		super.onPause();
	}

	@Override
	public void onDestroy() {
		clear();
		imageLoader.stop();
		if(this.name!=null){
			((Arabela)this.context).removeDir("/"+this.name);
		}
		this.context.unregisterReceiver(this.update);
		super.onDestroy();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = this.context.getMenuInflater();
		inflater.inflate(R.menu.listas_menu_productos_lst, menu);
		menu.setHeaderTitle(getString(R.string.opciones));
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.delete:
			AlertDialog.Builder delete = new AlertDialog.Builder(this.context);                 
			delete.setTitle(getString(R.string.confirmacion));  
			delete.setMessage(getString(R.string.eliminar));                

			delete.setPositiveButton(getString(R.string.si), new DialogInterface.OnClickListener() {  
				@Override
				public void onClick(DialogInterface dialog, int whichButton) {
					((MyFragmentActivity)ProductosListaF.this.context).trackEvents("Listas", "Eliminar Producto", ProductosListaF.this.lista.getProductos().get((int)info.id).name, 0);
					ProductosListaF.this.lista.removeProduct((int)info.id, ProductosListaF.this.context);
					((Arabela)ProductosListaF.this.context).dataChanged();
					updateData();               
				}  
			});  

			delete.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					updateData();
					return;   
				}
			});
			delete.show();
			break;
		default:
			return super.onContextItemSelected(item);
		}
		return true;
	}


	@Override
	public void updateData() {
		ArrayList <Product> products = this.lista.getProductos();
		this.t_adapter.clear();
		if(products != null && products.size() > 0){	
			tvTotalAmount.setText(getString(R.string.pedido_total)+" "+Utils.stringtoCurrency(getLista().getTotalAmount().toString()));
			for( Product product : products)
				this.t_adapter.add(product);
			this.t_adapter.notifyDataSetChanged();
			this.view.setVisibility(View.VISIBLE);
		}else {
			String NO_PRODUCT = getString(R.string.sin_productos);
			AlertDialog.Builder noProducts = new AlertDialog.Builder(this.context);                 
			noProducts.setTitle(getString(R.string.aviso));  
			noProducts.setMessage(NO_PRODUCT);    
			noProducts.setCancelable(false);

			noProducts.setPositiveButton(getString(R.string.no), new DialogInterface.OnClickListener() {  
				@Override
				public void onClick(DialogInterface dialog, int whichButton) {  
					((Arabela) ProductosListaF.this.context).removeFragment();
					return;                  
				}  
			});  

			noProducts.setNegativeButton(getString(R.string.si), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					((Arabela)ProductosListaF.this.context).setList(ProductosListaF.this.lista);
					Fragment fragment = new CategoriasF();
					((CategoriasF)fragment).setName("Categorias");
					((MyFragmentActivity)ProductosListaF.this.context).addDir("Categorias");
					((MyFragmentActivity)ProductosListaF.this.context).startNextFragment(fragment);
					return;   
				}
			});
			noProducts.show();
		}
	}

	class PedidoAdapter extends ArrayAdapter<Product> {

		private int textViewResourceId; 
		public PedidoAdapter(Context context, int textViewResourceId, ArrayList<Product> products ) {
			super(context , textViewResourceId, products);
			this.textViewResourceId = textViewResourceId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			View v = convertView;
			if (v == null || !(v.getTag() instanceof ViewHolder)) {
				LayoutInflater vi = LayoutInflater.from(ProductosListaF.this.context);
				v = vi.inflate(this.textViewResourceId, null);
				holder = new ViewHolder();
				holder.name = (TextView) v.findViewById(R.id.product_title);
				holder.price = (TextView)  v.findViewById(R.id.product_normal_price);
				holder.price_with_discount = (TextView)  v.findViewById(R.id.product_discount_price);
				holder.image=(ImageView)v.findViewById(R.id.product_image);
				holder.total = (TextView)  v.findViewById(R.id.product_total);
				holder.number=(TextView)v.findViewById(R.id.et_numero_productos);
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			final Product o = super.getItem(position);
			if (o != null){
				String  name = o.name!=null?o.name.trim():""; //Html.fromHtml( 
				holder.name.setText( Html.fromHtml(name ) );  //Formateando el texto en html
				holder.price.setText(Utils.stringtoCurrency(o.price));
				imageLoader.displayImage(o.thumbnail, holder.image, options,new MyImageLoadingListener(holder.image));
				holder.number.setText(Integer.toString(o.cantidad));
				if (o.price_with_discount != null){
					holder.price_with_discount.setText(Utils.stringtoCurrency(o.price_with_discount));
					holder.price_with_discount.setVisibility(View.VISIBLE);
					holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
					Double total =Double.parseDouble(o.price_with_discount)*o.cantidad;
					holder.total.setText(Utils.stringtoCurrency(total.toString()));
				}
				else {
					holder.price.setPaintFlags( holder.price.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
					holder.price_with_discount.setVisibility(View.INVISIBLE);
					Double total =Double.parseDouble(o.price)*o.cantidad;
					holder.total.setText(Utils.stringtoCurrency(total.toString()));
				}
				if(((Arabela)context).mTabManager.mTabHost.getCurrentTabTag().equals(StackType.NO_ENVIADOS.toString())){
					holder.number.setOnClickListener(new OnClickListener(){
						@Override
						public void onClick(View arg0) {
							setAmountProduct(o);
						}
					});
				}
			}
			return v;
		}
	}

	public void setAmountProduct(final Product o){
		AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
		builder.setTitle("Cantidad");
		builder.setSingleChoiceItems(items, o.cantidad-1, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				lista.setCantidad(o, item+1, ProductosListaF.this.context);
				if(((Arabela)context).mTabManager.mTabHost.getCurrentTabTag().equals(StackType.NO_ENVIADOS.toString())){
					FileSdStorage fileNoEnviados = new FileSdStorage("No_Enviados");
					ListasPojo listasNoEnviados = (ListasPojo)fileNoEnviados.loadObject(ProductosListaF.this.context);
					listasNoEnviados.remplaceList(lista);
					fileNoEnviados.saveObject(listasNoEnviados,ProductosListaF.this.context);
				}
				t_adapter.notifyDataSetChanged();
				dialog.dismiss();
				updateData();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		ProductosListaF.this.setType(ProductosListaF.Type.LISTA);
		if (position < 0)
			return;
		Product o = this.t_adapter.getItem(position);
		if (o != null){
			sonido.play(false);
			Fragment fragmnet = new ProductoF(o,webServices);
			((Arabela)this.context).addDir(o.name);
			clear();
			this.page=1;
			((Arabela) this.context).startNextFragment(fragmnet);
		}
	}

	public void sendPedido(Lista l){
		FileSdStorage fileEnviados = new FileSdStorage("Enviados");
		ListasPojo listasEnviados = (ListasPojo)fileEnviados.loadObject(this.context);
		if (listasEnviados == null)
			listasEnviados = new ListasPojo();
		listasEnviados.addList(l);
		fileEnviados.saveObject(listasEnviados,this.context);

		FileSdStorage fileNoEnviados = new FileSdStorage("No_Enviados");
		ListasPojo listasNoEnviados = (ListasPojo)fileNoEnviados.loadObject(this.context);
		listasNoEnviados.dropList(listasNoEnviados.getindex(l.getNombre()), context);
		fileNoEnviados.saveObject(listasNoEnviados,this.context);
	}

	public void recivePedido(Lista l){
		FileSdStorage fileRecibidos = new FileSdStorage("Recibidos");
		ListasPojo listasRecibidas = (ListasPojo)fileRecibidos.loadObject(this.context);
		if (listasRecibidas == null)
			listasRecibidas = new ListasPojo();
		listasRecibidas.addList(l);
		fileRecibidos.saveObject(listasRecibidas,this.context);

		FileSdStorage fileEnviados = new FileSdStorage("Enviados");
		ListasPojo listasEnviados = (ListasPojo)fileEnviados.loadObject(this.context);
		listasEnviados.dropList(listasEnviados.getindex(l.getNombre()), context);
		fileEnviados.saveObject(listasEnviados,this.context);
	}

	private OnClickListener agregarProducto = new OnClickListener (){
		@Override
		public void onClick(View v) {
			((Arabela)context).setList(getLista());
			Fragment fragmnet = new CategoriasF();
			((CategoriasF)fragmnet).setName("Categorias");
			((MyFragmentActivity)context).addDir("Categorias");
			((MyFragmentActivity)context).startNextFragment(fragmnet);
		}
	};

	private OnClickListener enviarLista = new OnClickListener (){
		@Override
		public void onClick(View v) {
			sendPedido(ProductosListaF.this.lista);
			((Arabela)context).removeFragment();
		}
	};

	private OnClickListener recibirLista = new OnClickListener (){
		@Override
		public void onClick(View v) {
			recivePedido(ProductosListaF.this.lista);
			((Arabela)context).removeFragment();
		}
	};

	private void clear(){
		this.t_adapter.clear();
		this.t_adapter.notifyDataSetChanged();
	}

	public void showProgressFooter(){
		this.pbFooter.setVisibility(View.VISIBLE);
	}

	public void hideprogressFooter(){
		this.pbFooter.setVisibility(View.INVISIBLE);
	}


	protected Runnable runGetProductsDB = new Runnable(){
		@Override
		public void run() {
			AnsType ansType = AnsType.UNKNOW;
			boolean reintent=false;
			try {
				String url;
				if (ProductosListaF.this.type == Type.PROMOCION){
					url = String.format(AppArabela.getInstance().getPrefs().loadData("URL_SERVER") + "productsPromo/%s/",  URLEncoder.encode(id,  "UTF-8")) ;
					url = Net.addParameter(url, "page", page.toString());
					products = (Products) Net.sendDataAndGetObjectCache(url, Products.class, null);
					ProductosListaF.this.webServices = true;
				}else{
					ProductosListaF.this.dba.open();
					products = ProductosListaF.this.dba.getProductos(ProductosListaF.this.id);
					ProductosListaF.this.dba.close();
				}
				ansType = AnsType.OK;
			} catch (HttpException e) {					
				reintent=Utils.getUrls();
				ansType = AnsType.NETWORK_PROBLEM;
			} catch (IOException e) {
				ansType = AnsType.NETWORK_PROBLEM;
				e.printStackTrace();
			} catch (OfflineException e) {
				ansType = AnsType.OFFLINE;
				e.printStackTrace();
			} catch (Exception e) {
				ansType = AnsType.UNKNOW;
				e.printStackTrace();
			}
			if(reintent==true)
				new Thread(null, ProductosListaF.this.runGetProductsDB, "runGetProducts").start();
			else{
				if(ProductosListaF.this.search==null&&(ProductosListaF.this.type == Type.PROMOCION||products.total==ProductosListaF.this.total)&&products.total>0)
					ProductosListaF.this.context.runOnUiThread(new ResGetProducts(products,ansType));
				else
					new Thread(null, ProductosListaF.this.runGetProducts, "runGetProducts").start();
			}
		}
	};
	protected Runnable runGetProducts = new Runnable(){
		@Override
		public void run() {
			ProductosListaF.this.webServices = true;
			AnsType ansType = AnsType.UNKNOW;
			boolean reintent=false;
			try {
				String url;
				url = AppArabela.getInstance().getPrefs().loadData("URL_SERVER") + "products/" ;
				url = Net.addParameter(url, "cat", id);
				url = Net.addParameter(url, "name", search);
				url = Net.addParameter(url, "page", page.toString());
				products = (Products) Net.sendDataAndGetObjectCache(url, Products.class, null);
				ansType = AnsType.OK;
			} catch (HttpException e) {					
				reintent=Utils.getUrls();
				ansType = AnsType.NETWORK_PROBLEM;
			} catch (IOException e) {
				ansType = AnsType.NETWORK_PROBLEM;
				e.printStackTrace();
			} catch (OfflineException e) {
				ansType = AnsType.OFFLINE;
				e.printStackTrace();
			} catch (Exception e) {
				ansType = AnsType.UNKNOW;
				e.printStackTrace();
			}
			if(reintent==true)
				new Thread(null, ProductosListaF.this.runGetProducts, "runGetProducts").start();
			else
				ProductosListaF.this.context.runOnUiThread(new ResGetProducts(products,ansType));
		}
	};

	class  ResGetProducts implements Runnable {

		List <Product> productos;
		com.pagatodo.catalogoarabela.pojos.Error error;
		AnsType ansType;
		public ResGetProducts (Products products,AnsType ansType){
			if ( products != null){
				this.productos=null;
				ProductosListaF.this.page = products.page;
				ProductosListaF.this.totalPages = products.total_pages;
				this.productos = products.products;
				this.error = products.error;
			} 
			this.ansType = ansType;
		}
		@Override
		public void run() {

			hideprogressFooter();
			switch (this.ansType) {
			case OK:
				if(this.productos != null && this.productos.size() > 0 && this.error == null){	
					for( Product product : this.productos){
						ProductosListaF.this.t_adapter.add(product);
					}
					ProductosListaF.this.view.setVisibility(View.VISIBLE);
				}
				else {

					if(ProductosListaF.this.page==1){	
						((Arabela) context).removeFragment();	
					}


					if (error == null){
						error = new com.pagatodo.catalogoarabela.pojos.Error();

						error.title = getString(R.string.lo_sentimos);
						error.message = getString(R.string.no_product);
					}

					UI.showAlertDialog( error.title, error.message, getString(R.string.ok), context, null).show();

				}
				InputMethodManager imm = (InputMethodManager)ProductosListaF.this.context.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(ProductosListaF.this.view.getWindowToken(), 0);
				ProductosListaF.this.t_adapter.notifyDataSetChanged();
				ProductosListaF.this.refresh=false;
				break;
			case OFFLINE:
				if(ProductosListaF.this.resume==true){
					if(ProductosListaF.this.page==1){
						( (Arabela) ProductosListaF.this.context).removeFragment();
					}
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_sin_internet), getString(R.string.ok), ProductosListaF.this.context, null).create().show();
				}
				break;

			case NETWORK_PROBLEM:
				if(ProductosListaF.this.resume==true){
					if(ProductosListaF.this.page==1){
						( (Arabela) ProductosListaF.this.context).removeFragment();
					}
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_error), getString(R.string.ok), ProductosListaF.this.context, null).create().show();
				}
				break;
			case UNKNOW:
			case DATA_PROBLEM:
			default:
				if(ProductosListaF.this.resume==true){
					if(ProductosListaF.this.page==1){
						( (Arabela) ProductosListaF.this.context).removeFragment();
					}
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_datos_corruptos), getString(R.string.ok), ProductosListaF.this.context, null).create().show();
				}
				break;
			}
		}
	}

	@Override
	public String getCategory() {
		return  this.id;
	}

	private static class ViewHolder {
		TextView name;
		TextView price;
		TextView price_with_discount;
		ImageView image;
		TextView total;
		TextView number;
	}

	class MyImageLoadingListener implements ImageLoadingListener {
		private ImageView image;
		public MyImageLoadingListener (ImageView image ){
			this.image = image;
		}
		@Override
		public void onLoadingCancelled() {}
		@Override
		public void onLoadingComplete() {}
		@Override
		public void onLoadingFailed(FailReason arg0) {
			this.image.setImageResource(R.drawable.stub_2);
		}
		@Override
		public void onLoadingStarted() {}

	};
	
	private BroadcastReceiver update = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			ListasPojo listas = (ListasPojo) intent.getSerializableExtra("Listas");
			ProductosListaF.this.lista = listas.getLista(listas.getindex(ProductosListaF.this.lista.getNombre()));
			updateData();			
		}
	};
}