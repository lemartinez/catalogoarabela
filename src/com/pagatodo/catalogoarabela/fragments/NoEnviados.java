package com.pagatodo.catalogoarabela.fragments;

import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pagatodo.catalogoarabela.Arabela;
import com.pagatodo.catalogoarabela.R;
import com.pagatodo.catalogoarabela.pojos.Product;
import com.pagatodo.persistencia.FileSdStorage;

public class NoEnviados extends ListasF {
	
	private TextView msgAddProduct;
	private boolean inCatalogo;
	
	public NoEnviados (){
		this(null);
	}
	public NoEnviados (Product product){
		super.product = product;
	}
	public NoEnviados (Product product,boolean inCatalogo){
		super.product = product;
		this.inCatalogo = inCatalogo;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		super.fileSdStorage =new FileSdStorage("No_Enviados");
		this.msgAddProduct=(TextView) this.view.findViewById(R.id.msg_add_to_list);
		if(this.inCatalogo){
			((Arabela)super.context).hideBarSearch();
			msgAddProduct.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
		}else{
			msgAddProduct.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 0));
		}
	}

	
	@Override
	public void onPause() {
		if(!((Arabela)super.context).inPedidos()){
			((Arabela)super.context).ShowBarSearch();
			msgAddProduct.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 0));
		}
		super.onPause();
	}
	
}
