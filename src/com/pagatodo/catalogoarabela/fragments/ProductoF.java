package com.pagatodo.catalogoarabela.fragments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.facebook.FacebookException;
import com.facebook.Session;
import com.facebook.Session.OpenRequest;
import com.facebook.SessionState;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.pagatodo.catalogoarabela.AppArabela;
import com.pagatodo.catalogoarabela.Arabela;
import com.pagatodo.catalogoarabela.R;
import com.pagatodo.catalogoarabela.pojos.Imagenes;
import com.pagatodo.catalogoarabela.pojos.Product;
import com.pagatodo.catalogoarabela.pojos.UrlShortener;
import com.pagatodo.catalogoarabela.recursos.Recursos.AnsType;
import com.pagatodo.exceptions.HttpException;
import com.pagatodo.exceptions.OfflineException;
import com.pagatodo.interfaces.Update;
import com.pagatodo.net.Net;
import com.pagatodo.ui.UI;
import com.pagatodo.utils.Utils;

@SuppressLint({ "ValidFragment", "ValidFragment" })
public class ProductoF extends Fragment implements Update {

	private String id = null;
	private TextView tvTitulo;
	private TextView tvDescripcion;
	private TextView tvSKU;
	private TextView tvPrecio;
	private TextView tvPrecioDescuento;
	private Button btnAgregarListas;
	private ImageButton btnFacebook;
	private ImageButton btnTwitter;
	private ImageView imgProducto;
	private DisplayImageOptions options;
	private ImageLoader imageLoader = ImageLoader.getInstance();
	private ProgressDialog m_ProgressDialog = null;
	private Activity context;
	private View view = null;
	private Product product = null;
	private String urlShort;
	private String barcode = null;
	private String name = null;
	private Boolean webServices;
	private Session.StatusCallback statusCallback = new SessionStatusCallback();


	public ProductoF (Product product,Boolean webServices) {
		this.product = product;
		this.id = product.sku;
		this.webServices = webServices;

	}

	public ProductoF (String id_category) {
		this.id = id_category;
	}

	public ProductoF (Product product) {
		this.product = product;
		this.id = product.sku;

	}

	public ProductoF (){}

	public void setBarcode(String barcode){
		this.barcode = barcode;
		return;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		this.context = getActivity();

		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.stub_)
		.showImageForEmptyUrl(R.drawable.stub_)
		.cacheInMemory()
		.cacheOnDisc()
		.build();
		imageLoader.init(ImageLoaderConfiguration.createDefault(context));

		if(savedInstanceState!=null){
			if (savedInstanceState.containsKey("id"))
				id = savedInstanceState.getString("id");
			if (savedInstanceState.containsKey("producto"))
				product=(Product) savedInstanceState.getSerializable("producto");
			if (savedInstanceState.containsKey("barcode"))
				barcode= savedInstanceState.getString("barcode");
			if (savedInstanceState.containsKey("webServices"))
				webServices= savedInstanceState.getBoolean("webServices");
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (id != null ) outState.putSerializable("id", id);
		if (product != null ) outState.putSerializable("producto", product);
		if (barcode != null ) outState.putString("barcode", barcode);
		if (webServices != null ) outState.putBoolean("webServices", webServices);
		Session session = Session.getActiveSession();
		Session.saveSession(session, outState);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
	}

	@SuppressWarnings("cast")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		this.view = (View) inflater.inflate(R.layout.producto, container, false);
		this.view.setVisibility(View.INVISIBLE);

		this.tvTitulo = (TextView) this.view.findViewById(R.id.pro_titulo);
		this.tvDescripcion = (TextView) this.view.findViewById(R.id.pro_descripcion_txt);
		this.tvSKU = (TextView) this.view.findViewById(R.id.pro_sku);
		this.tvPrecio = (TextView) this.view.findViewById(R.id.pro_precio_normal);
		this.tvPrecioDescuento = (TextView) this.view.findViewById(R.id.pro_precio_descuento);

		this.btnAgregarListas = (Button)  this.view.findViewById(R.id.pro_agregar_listas);
		this.btnFacebook = (ImageButton)  this.view.findViewById(R.id.bt_facebook);
		this.btnTwitter = (ImageButton)  this.view.findViewById(R.id.bt_twitter);
		this.imgProducto = (ImageView) this.view.findViewById(R.id.img_product);

		this.btnAgregarListas.setOnClickListener(this.agregarListas);
		this.btnFacebook.setOnClickListener(this.compartirFacebook);
		this.btnTwitter.setOnClickListener(this.compartirTwitter);

		this.imgProducto.setOnClickListener(this.galeria);

		updateData();
		Session session = Session.getActiveSession();
		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(getActivity(), null, statusCallback, savedInstanceState);
			}
		}
		return this.view;
	}


	@Override
	public void onResume() {
		super.onResume();
		if (this.product != null){
			this.tvTitulo.setText(this.product.name!=null?this.product.name.trim():"");
			this.tvDescripcion.setText(this.product.description!=null? this.product.description.trim():"");
			if (!((Arabela)this.context).inPedidos()){
				this.tvPrecio.setVisibility(View.VISIBLE);
				this.tvPrecioDescuento.setVisibility(View.VISIBLE);
				this.tvPrecio.setText(com.pagatodo.utils.Utils.formatNumberToMoney(this.product.price));
				this.tvSKU.setText("SKU " + this.product.sku);
				if (this.product.price_with_discount != null){
					tvPrecio.setPaintFlags(ProductoF.this.tvPrecio.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG); 
					this.tvPrecioDescuento.setText( com.pagatodo.utils.Utils.formatNumberToMoney(this.product.price_with_discount ));
				}
				else{
					this.tvPrecioDescuento.setText("");
				}
			}
			imageLoader.displayImage(this.product.thumbnail, this.imgProducto, options, new MyImageLoadingListener(imgProducto));
			ProductoF.this.view.setVisibility(View.VISIBLE);
			//this.dba.open();
		}
	}



	@Override
	public void onDestroy() {
		super.onDestroy();
		if(this.name!=null){
			((Arabela)this.context).removeDir("/"+this.name);
		}else{
			if(this.barcode!=null){
				if(this.product!=null)
					((Arabela)this.context).removeDir("/Busqueda="+this.product.name+"/"+this.product.name);
			}
			else{
				((Arabela)this.context).removeDir("/"+this.product.name);
			}
		}
		imageLoader.stop();
	}

	public void setName(String name){
		this.name=new String(name);
	}


	private OnClickListener agregarListas = new OnClickListener(){
		@Override
		public void onClick(View v) {
			((Arabela)ProductoF.this.context).trackEvents("Productos", "Agregar a Lista", ProductoF.this.product.name, 0);

			if (((Arabela)ProductoF.this.context).inPedidos()){
				((Arabela)context).getList().addProduct(ProductoF.this.product,context);
				((Arabela)context).dataChanged();
				AlertDialog.Builder confirm = new AlertDialog.Builder(context);                 
				confirm.setTitle(getString(R.string.aviso));  
				confirm.setMessage(getString(R.string.producto_agregado));                

				confirm.setPositiveButton(getString(R.string.cerrar), new DialogInterface.OnClickListener() {  
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {}  
				});  

				confirm.setNegativeButton(getString(R.string.listas_back), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						((Arabela)context).removetoProductosListaF();
						return;   
					}
				});
				confirm.show();
			}else {
				Fragment fragmnet = new NoEnviados(ProductoF.this.product,true);
				((Arabela)ProductoF.this.context).startNextFragment(fragmnet);
			}
		}
	};


	private OnClickListener compartirFacebook = new OnClickListener(){
		@Override
		public void onClick(View v) {
			if (ProductoF.this.product != null){
				shareFacebook();
			}
		}
	};

	private OnClickListener compartirTwitter = new OnClickListener(){

		@Override
		public void onClick(View v) {
			if (ProductoF.this.product != null){
				((Arabela)ProductoF.this.context).trackEvents("Productos", "Compartir", ProductoF.this.product.name, 0);
				String url;				
				if(ProductoF.this.product.url==null||ProductoF.this.product.url.equals("NULL"))
					url = "www.pagatodo.com";
				else
					url = ProductoF.this.urlShort;
				((Arabela)ProductoF.this.context).setMessageToShare(url);
				((Arabela)ProductoF.this.context).sharetw();
			}

		}
	};	 

	OnClickListener galeria = new OnClickListener(){
		@Override
		public void onClick(View v) {
			Bundle bundle = new Bundle();
			if (product.images != null && product.images.size() >0){
				bundle.putSerializable("IMAGENES", new ArrayList<Imagenes>(product.images));
				startGalery(bundle);
			}else{
				if(product.image_main!=null&&!product.image_main.equals("")){
					Imagenes img=new Imagenes();
					List<Imagenes> images =new ArrayList<Imagenes>();
					img.id="image_main";
					img.url=product.image_main;
					images.add(img);
					bundle.putSerializable("IMAGENES", new ArrayList<Imagenes>(images));
					startGalery(bundle);
				}else{
					Imagenes img=new Imagenes();
					List<Imagenes> images =new ArrayList<Imagenes>();
					img.id="1";
					img.url=product.thumbnail;
					images.add(img);
					bundle.putSerializable("IMAGENES", new ArrayList<Imagenes>(images));
					startGalery(bundle);
				}
			}
		}
	};
	private void startGalery(Bundle bundle){
		Fragment fragment = new GalleryF();
		fragment.setArguments(bundle);
		((Arabela)ProductoF.this.context).startNextFragment(fragment);
		((Arabela)ProductoF.this.context).addDir("Galeria");
	}

	@Override
	public void updateData(){
		new Thread(null, this.runGetProduct, "runGetProduct").start();
		if (!((Arabela)ProductoF.this.context).inPedidos()){
			this.m_ProgressDialog = ProgressDialog.show(this.context, getString(R.string.espere), getString(R.string.obteniendo), true,true,
					new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {			
					( (Arabela) ProductoF.this.context).removeFragment();
				}
			});
		}
	}


	/*
	 *Runnables para ejecutar los webServices getCategories y getProducts 
	 *
	 */


	//Background getBalance()
	private Runnable runGetProduct = new Runnable(){
		@Override
		public void run() {

			Product product_ = null;
			AnsType ansType = AnsType.UNKNOW;
			boolean reintent=false;
			try {
				if(ProductoF.this.product==null||ProductoF.this.webServices==true){
					String url = AppArabela.getInstance().getPrefs().loadData("URL_SERVER") + "product/";
					if (barcode == null){
						url = Net.addParameter(url, "search", "sku");
						url = Net.addParameter(url, "value", id);
					}
					else {
						url = Net.addParameter(url, "search", "barcode");
						url = Net.addParameter(url, "value", barcode);
					}
					if (((Arabela)ProductoF.this.context).inPedidos()){
						product_ = (Product) Net.sendDataAndGetObject(url, Product.class, null);
					}
					else{
						product_ = (Product) Net.sendDataAndGetObjectCache(url, Product.class, null);	
					}
					ansType = AnsType.OK;
					if(product_==null)
						throw new Exception();
					ProductoF.this.product = product_;
				}
				ansType = AnsType.OK;
				if(ProductoF.this.barcode!=null){
					((Arabela)ProductoF.this.context).addDir("Busqueda="+ProductoF.this.product.name+"/"+ProductoF.this.product.name);
					((Arabela)ProductoF.this.context).trackEvents("Productos", "Busqueda", ProductoF.this.product.name, 1);
				}
				if ( ProductoF.this.product.url != null){
					new Thread(null, ProductoF.this.runGetUrlShort, "runGetUrlShort").start();
				}

			} catch (HttpException e) {					
				reintent=Utils.getUrls();
				ansType = AnsType.NETWORK_PROBLEM;
			} catch (IOException e) {
				ansType = AnsType.NETWORK_PROBLEM;
				e.printStackTrace();
			} catch (OfflineException e) {
				ansType = AnsType.OFFLINE;

				e.printStackTrace();
			} catch (Exception e) {
				ansType = AnsType.UNKNOW;
				e.printStackTrace();
			}
			if(reintent==true)
				new Thread(null, ProductoF.this.runGetProduct, "runGetProduct").start();
			else
				ProductoF.this.context.runOnUiThread(new ResGetProduct(ProductoF.this.product, ansType));
		}
	};

	private Runnable runGetUrlShort= new Runnable() {
		@Override
		public void run() {
			UrlShortener urlShort_ = null;
			if ( ProductoF.this.product.url.equals("NULL") == false){
				try {
					urlShort_= Net.getUrlShort(ProductoF.this.product.url);
					if (urlShort_ != null && urlShort_.id != null){
						ProductoF.this.urlShort= urlShort_.id;
					}
				} catch (Exception e) {
					ProductoF.this.urlShort = null;
				}
			}
		}
	};

	class  ResGetProduct implements Runnable {
		Product product_;
		AnsType ansType;
		com.pagatodo.catalogoarabela.pojos.Error error;
		public ResGetProduct (Product product,AnsType ansType){
			if ( product != null){
				this.product_ = product; 
				this.error = product.error;
			}
			this.ansType = ansType;
		}
		@Override
		public void run() {
			if (!((Arabela)ProductoF.this.context).inPedidos()){
				ProductoF.this.m_ProgressDialog.dismiss();
			}
			switch (this.ansType) {
			case OK:
				if(this.product_ != null && this.product_.error == null){
					ProductoF.this.tvTitulo.setText(this.product_.name!=null?this.product_.name.trim():"");
					ProductoF.this.tvDescripcion.setText(this.product_.description != null? this.product_.description.trim() :"");
					ProductoF.this.tvSKU.setText("SKU " + this.product_.sku);
					//					if (!(context instanceof Listas)){
					tvPrecio.setVisibility(View.VISIBLE);
					ProductoF.this.tvPrecio.setText(Html.fromHtml(com.pagatodo.utils.Utils.formatNumberToMoney(this.product_.price) ));
					if (product.price_with_discount != null){
						ProductoF.this.tvPrecio.setPaintFlags(ProductoF.this.tvPrecio.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG); //rayar texto
						tvPrecioDescuento.setText( Utils.stringtoCurrency(product.price_with_discount ));
						tvPrecioDescuento.setVisibility(View.VISIBLE);
					}
					else{
						tvPrecioDescuento.setText("");
					}
					//					}
					if(product.image_main!=null&&!product.image_main.equals("")){
						imageLoader.displayImage(product.image_main, imgProducto, options,new MyImageLoadingListener(imgProducto));
						
					}
					else{
						imageLoader.displayImage(product.thumbnail, imgProducto, options, new MyImageLoadingListener(imgProducto));
						
					}
					ProductoF.this.view.setVisibility(View.VISIBLE);

				}
				else {
					((Arabela)ProductoF.this.context).removeFragment();
					if (error == null){
						error.title = getString(R.string.lo_sentimos);
						error.message = getString(R.string.codigo_no_encontrado);
					}
					UI.showAlertDialog( error.title, error.message, getString(R.string.ok), context, null).show();
				}
				break;

			case OFFLINE:
				if (!((Arabela)ProductoF.this.context).inPedidos()){
					((Arabela)ProductoF.this.context).removeFragment();
					UI.showAlertDialog(getString(R.string.lo_sentimos), getString(R.string.monedero_sin_internet), getString(R.string.ok), context, null).create().show();
				}
				break;

			case NETWORK_PROBLEM:
				if (!((Arabela)ProductoF.this.context).inPedidos()){
					((Arabela)ProductoF.this.context).removeFragment();
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_error), getString(R.string.ok),context, null).create().show();
				}
				break;

			case UNKNOW:
			case DATA_PROBLEM:
			default:
				if (!((Arabela)ProductoF.this.context).inPedidos()){
					((Arabela)ProductoF.this.context).removeFragment();
					UI.showAlertDialog(getString(R.string.lo_sentimos), getString(R.string.monedero_datos_corruptos), getString(R.string.ok),context, null).create().show();
				}
				break;	
			}
		}
	}

	class MyImageLoadingListener implements ImageLoadingListener {
		private ImageView image;
		public MyImageLoadingListener (ImageView image ){
			this.image = image;
		}
		@Override
		public void onLoadingCancelled() {}
		@Override
		public void onLoadingComplete() {
			this.image.setScaleType(ScaleType.FIT_CENTER);
		}
		@Override
		public void onLoadingFailed(FailReason arg0) {
			this.image.setImageResource(R.drawable.stub_2);
		}
		@Override
		public void onLoadingStarted() {
			this.image.setScaleType(ScaleType.CENTER);
		}
	}

	/****Facebook API 3.0****/
	private void shareFacebook() {
		Session session = Session.getActiveSession();
		if (session == null) {
			session = new Session(getActivity());
		}
		Session.setActiveSession(session);
		if (!session.isOpened() && !session.isClosed()) {
			OpenRequest open = new OpenRequest(this).setCallback(statusCallback);
			List<String> permission = new ArrayList<String>();
			permission.add("publish_actions");
			open.setPermissions(permission);
			session.openForPublish(open);
		} else {
			Session.openActiveSession(getActivity(), this, true, statusCallback);
		}
	}

	private void publishFeedDialog() {
		String url;			
		if(ProductoF.this.product.url==null||ProductoF.this.product.url.equals("NULL"))
			url = "www.marti.mx";
		else
			url = ProductoF.this.product.url;
		Bundle params = new Bundle();
		params.putString("name", ProductoF.this.product.name);
		params.putString("caption", ProductoF.this.product.price);
		params.putString("description", ProductoF.this.product.description);
		params.putString("link", url);
		if(ProductoF.this.product.thumbnail!=null)
			params.putString("picture", ProductoF.this.product.thumbnail);

		WebDialog feedDialog = (
				new WebDialog.FeedDialogBuilder(getActivity(),Session.getActiveSession(),params))
				.setOnCompleteListener(new OnCompleteListener() {
					@Override
					public void onComplete(Bundle values, FacebookException error) {
						if (error == null) {
							final String postId = values.getString("post_id");
							Log.d("POST_ID", postId);
						} 
					}
				})
				.build();
		feedDialog.show();
	}

	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			if (session.isOpened()) {
				publishFeedDialog();
			} 
		}
	}
}