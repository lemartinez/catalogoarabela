package com.pagatodo.catalogoarabela.fragments;

import android.os.Bundle;
import android.view.View;

import com.pagatodo.persistencia.FileSdStorage;

public class Enviados extends ListasF {
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		super.fileSdStorage =new FileSdStorage("Enviados");
		super.footer.setVisibility(View.GONE);
	}
}
