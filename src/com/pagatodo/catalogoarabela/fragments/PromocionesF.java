package com.pagatodo.catalogoarabela.fragments;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.pagatodo.catalogoarabela.Arabela;
import com.pagatodo.catalogoarabela.R;
import com.pagatodo.catalogoarabela.pojos.Banner;
import com.pagatodo.catalogoarabela.pojos.Connections;
import com.pagatodo.catalogoarabela.pojos.Promotions;
import com.pagatodo.catalogoarabela.recursos.Recursos.AnsType;
import com.pagatodo.exceptions.HttpException;
import com.pagatodo.exceptions.OfflineException;
import com.pagatodo.net.Net;
import com.pagatodo.ui.UI;
import com.pagatodo.utils.Utils;

public class PromocionesF extends Fragment {


	private Activity context;
	private boolean resume=false;
	private Promotions promo;

	private ImageView bannerA;
	private ImageView bannerB;

	private DisplayImageOptions options;
	private ImageLoader imageLoader = ImageLoader.getInstance();


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		this.context = getActivity();
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.stub)
		.cacheInMemory()
		.cacheOnDisc()
		.build();
		imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		updateData();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.catalogo, container, false);
		bannerA=(ImageView) v.findViewById(R.id.bannerA);
		bannerB=(ImageView) v.findViewById(R.id.bannerB);
		return v;
	}


	@Override
	public void onDestroy(){
		imageLoader.stop();
		super.onDestroy();
	}


	@Override
	public void onResume() {
		super.onResume();
		((Arabela)getActivity()).hideBarSearch();
		resume=true;
		if(this.promo!=null){
			fillTopBanner(promo.topBanners);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		((Arabela)getActivity()).ShowBarSearch();
		resume=false;
	}


	public void updateData(){
		new Thread(null, this.runGetPromotions, "getPromotions").start();
	}

	//Llenado las promociones superiores

	private void fillTopBanner(List<Banner> banners){

		imageLoader.displayImage(banners.get(0).image, bannerA, options, new MyImageLoadingListener(bannerA));
		addListener(bannerA, banners.get(0));

		imageLoader.displayImage(banners.get(1).image, bannerB, options,  new MyImageLoadingListener(bannerB)); //, new MyImageLoadingListener(bannerB, 0.8768f)
		addListener(bannerB, banners.get(1));

		return ;
	}

	/*
	 * Agregando listsners seg�n el tipo de promoci�n a la vista   
	 *
	 **/

	private  void addListener(View view , Banner banner){

		view.setClickable(true);

		if (banner.type.equals("url")){
			view.setOnClickListener( new ListenerUrl(banner.value,banner.name) );
		}
		else if (banner.type.equals("category")){
			view.setOnClickListener( new ListenerCategoria(banner.value,banner.name) );
		}
		else if (banner.type.equals("product_list")){
			view.setOnClickListener( new ListenerProductsList(banner.value,banner.name) );
		}

		else if (banner.type.equals("product_list_cat")){
			view.setOnClickListener( new ListenerProductsListCategory(banner.value,banner.name) );
		}

		else if (banner.type.equals("product")){
			view.setOnClickListener(  new ListenerProductDetail(banner.value,banner.name) );
		}

		return;
	}

	//Listener para promocion de URL

	class ListenerUrl implements OnClickListener {
		String url = null;
		String name=null;
		ListenerUrl (String url,String name){
			this.url = url;
			this.name=name;
		}

		@Override
		public void onClick(View v) {
			Fragment fragmnet = new WebViewF(this.url,this.name);
			((MyFragmentActivity)PromocionesF.this.context).addDir(this.name+"/"+this.url);
			((MyFragmentActivity)PromocionesF.this.context).startNextFragment(fragmnet);

		}
	}

	//Listener para promoci�n de lista de categorias

	class ListenerCategoria implements OnClickListener {

		String value = null;
		String name=null;
		ListenerCategoria (String value,String name){
			this.value = value;
			this.name=name;
		}
		@Override
		public void onClick(View v) {
			CategoriasF fragment = new CategoriasF(value);
			fragment.setName(this.name);
			((MyFragmentActivity)PromocionesF.this.context).addDir(this.name);
			((MyFragmentActivity)PromocionesF.this.context).startNextFragment(fragment);

		}
	}


	//Listener para promoci�n de lista de productos

	class ListenerProductsList implements OnClickListener {

		String value = null;
		String name=null;
		ListenerProductsList (String value,String name){
			this.value = value;
			this.name = name;
		}
		@Override
		public void onClick(View v) {
			ProductosF fragment = new ProductosF(value);
			fragment.setName(this.name);
			fragment.setType(ProductosF.Type.PROMOCION);
			((MyFragmentActivity)PromocionesF.this.context).addDir(this.name);
			((MyFragmentActivity)PromocionesF.this.context).startNextFragment(fragment);

		}
	}

	//Listener para promoci�n de lista de productos de una categoria
	class ListenerProductsListCategory implements OnClickListener {

		String value = null;
		String name=null;
		ListenerProductsListCategory (String value,String name){
			this.value = value;
			this.name = name;
		}
		@Override
		public void onClick(View v) {
			ProductosF fragment = new ProductosF(value);
			fragment.setName(this.name);
			fragment.setType(ProductosF.Type.NORMAL);
			((MyFragmentActivity)PromocionesF.this.context).addDir(this.name);
			((MyFragmentActivity)PromocionesF.this.context).startNextFragment(fragment);

		}
	}


	//Listener para detalle producto
	class ListenerProductDetail implements OnClickListener {

		String value = null;
		String name = null;
		ListenerProductDetail (String value,String name){
			this.value = value;
			this.name = name;
		}
		@Override
		public void onClick(View v) {
			ProductoF fragment = new ProductoF(value);
			fragment.setName(this.name+"/"+this.value);
			((MyFragmentActivity)PromocionesF.this.context).addDir(this.name+"/"+this.value);
			((MyFragmentActivity)PromocionesF.this.context).startNextFragment(fragment);

		}
	}
	
	private Runnable runGetPromotions= new Runnable(){
		@Override
		public void run() {
			AnsType ansType = AnsType.UNKNOW;
			boolean reintent=false;
			Promotions promotions = null;
			Connections c = Utils.getCurrentConnections();
			final String url = c.URL_SERVER+"promotionsCatalogoArabela/";//URL_SERVER + "promotions/";
			try {			
				promotions = (Promotions) Net.sendDataAndGetObject(url,Promotions.class,null);
				ansType = AnsType.OK;
			} catch (HttpException e) {	
				Utils.setCurrentsURL(c);
				reintent=Utils.getUrls();
				ansType = AnsType.NETWORK_PROBLEM;
			} catch (IOException e) {
				ansType = AnsType.NETWORK_PROBLEM;
				e.printStackTrace();
			} catch (OfflineException e) {
				ansType = AnsType.OFFLINE;
				e.printStackTrace();
			} catch (Exception e) {
				ansType = AnsType.UNKNOW;
				e.printStackTrace();
			}
			if(reintent==true)
				new Thread(null, PromocionesF.this.runGetPromotions, "getPromotions").start();
			else
				PromocionesF.this.context.runOnUiThread(new ResGetPromotions(promotions,ansType));
		}
	};
	
	class  ResGetPromotions implements Runnable {

		Promotions promotions;
		AnsType ansType;
		public ResGetPromotions (Promotions promotions,AnsType ans){
			if ( promotions != null)
				this.promotions = promotions; 	
			if (ans!=null)
				this.ansType=ans;
			else
				this.ansType=AnsType.UNKNOW;

		}
		@Override
		public void run() {
				switch (ansType) {
				case OK:
					if(promotions != null&&promotions.error==null){
						PromocionesF.this.promo=promotions;
						fillTopBanner(promotions.topBanners);
					}else{
						if(promotions != null&&promotions.error!=null){
							UI.showAlertDialog(promotions.error.title, promotions.error.message, getString(R.string.ok), PromocionesF.this.context, null).show();
						}
					}
					break;
				case OFFLINE:
					if(PromocionesF.this.resume==true)
						UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_sin_internet), getString(R.string.ok), PromocionesF.this.context, null).create().show();
					break;
				case NETWORK_PROBLEM:
					if(PromocionesF.this.resume==true)
						UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_error), getString(R.string.ok), PromocionesF.this.context, null).create().show();
					break;
				case UNKNOW:
				case DATA_PROBLEM:
				default:
					if(PromocionesF.this.resume==true)
						UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_datos_corruptos), getString(R.string.ok), PromocionesF.this.context, null).create().show();
					break;
				}
		}
	}


	class MyImageLoadingListener implements ImageLoadingListener {
		private ImageView image;

		public MyImageLoadingListener (ImageView image){
			this.image = image;
		}
		@Override
		public void onLoadingCancelled() {}
		@Override
		public void onLoadingComplete() {

			this.image.setScaleType(ScaleType.FIT_CENTER);
		}
		@Override
		public void onLoadingFailed(FailReason arg0) {
			this.image.setImageResource(R.drawable.stub_);
		}
		@Override
		public void onLoadingStarted() {
			this.image.setScaleType(ScaleType.CENTER);
		}
	};
}