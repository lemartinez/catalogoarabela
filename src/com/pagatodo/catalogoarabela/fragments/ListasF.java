package com.pagatodo.catalogoarabela.fragments;

import static com.pagatodo.catalogoarabela.recursos.Recursos.DATA_CHANGED_LISTS;
import static com.pagatodo.catalogoarabela.recursos.Recursos.UPDATE_DATA;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.pagatodo.catalogoarabela.Arabela;
import com.pagatodo.catalogoarabela.R;
import com.pagatodo.catalogoarabela.fragments.MyFragmentActivity.StackType;
import com.pagatodo.catalogoarabela.pojos.Lista;
import com.pagatodo.catalogoarabela.pojos.ListasPojo;
import com.pagatodo.catalogoarabela.pojos.Product;
import com.pagatodo.catalogoarabela.recursos.Recursos;
import com.pagatodo.interfaces.Update;
import com.pagatodo.persistencia.FileSdStorage;
import com.pagatodo.ui.UI;
import com.pagatodo.utils.Utils;

public class ListasF extends SherlockListFragment implements Update{

	private ImageButton btnCreate;
	protected Activity context;
	private ListsAdapter l_adapter;
	protected RelativeLayout footer;
	private ListasPojo listas;
	protected FileSdStorage fileSdStorage;

	protected View view = null;
	protected Product product;

	public ListasF (){
		this(null);
	}

	public ListasF (Product product){
		this.product = product;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		if(savedInstanceState!=null){
			if (savedInstanceState.containsKey("product"))
				product = (Product) savedInstanceState.getSerializable("product");
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		if (product != null ) outState.putSerializable("product", product);
		super.onSaveInstanceState(outState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		this.context = getActivity();
		this.view = inflater.inflate(R.layout.pedidos, container, false);
		return this.view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.btnCreate = (ImageButton) this.view.findViewById(R.id.btn_nuevo_pedido);
		this.footer = (RelativeLayout) this.view.findViewById(R.id.footer);
		this.btnCreate.setOnClickListener(this.creaList);
		if (getListAdapter() == null){
			this.l_adapter = new ListsAdapter( R.layout.pedido_item, new ArrayList<Lista>());
			setListAdapter(this.l_adapter); 
		}
		registerForContextMenu(getListView());
		
		IntentFilter filter = new IntentFilter(DATA_CHANGED_LISTS);
		this.context.registerReceiver(this.dataChanged, filter);
		IntentFilter updatedata = new IntentFilter(UPDATE_DATA);
		this.context.registerReceiver(this.update, updatedata);
	}

	@Override
	public void onResume() {
		super.onResume();
		loadDataFromFile();
		updateData();		
	}

	@Override
	public void onStart() {
		super.onStart();
		this.btnCreate.requestFocus();
	}

	@Override
	public void onPause() {
		super.onPause();
		saveDataToFile();
	}	


	@Override
	public void onDestroy() {
		super.onDestroy();
		this.context.unregisterReceiver(this.dataChanged);
		this.context.unregisterReceiver(this.update);
	}

	private void loadDataFromFile(){
		this.listas = (ListasPojo)this.fileSdStorage.loadObject(this.context);
		if (this.listas == null)
			this.listas = new ListasPojo();
	}

	private void saveDataToFile(){
		this.fileSdStorage.saveObject(this.listas,this.context);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Lista o = (Lista) getListAdapter().getItem(position);
		if (this.product != null ){
			((MyFragmentActivity)this.context).trackEvents("Productos", "Agregar a Lista", this.product.name, 0);
			addProduct(position);
		}
		else if (o != null){
			clear();
			ProductosListaF fragment = new ProductosListaF();
			fragment.setLista(o);
			((MyFragmentActivity)this.context).startNextFragment(fragment);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = this.context.getMenuInflater();
		if(((Arabela)context).mTabManager.mTabHost.getCurrentTabTag().equals(StackType.NO_ENVIADOS.toString()))
			inflater.inflate(R.menu.listas_menu, menu);
		else
			inflater.inflate(R.menu.pedidos_menu, menu);
		menu.setHeaderTitle(getString(R.string.opciones));
	}


	@Override
	public boolean onContextItemSelected(MenuItem item) {
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.edit:
			String nombre = ListasF.this.listas.getLista((int)info.id).getNombre();
			final AlertDialog.Builder alert = new AlertDialog.Builder(this.context);                 
			alert.setTitle(getString(R.string.listas_cambiar));  
			alert.setMessage(getString(R.string.lista_nuevo_nombre));   
			final EditText input = new EditText(this.context); 
			input.setText(nombre);
			input.selectAll();
			alert.setView(input);

			InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

			alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {  
				@Override
				public void onClick(DialogInterface dialog, int whichButton) {  
					String value = input.getText().toString();

					if (value.equals("")){
						UI.showAlertDialog(getString(R.string.listas_upps),getString(R.string.listas_error_nombre_vacio), getString(R.string.ok), ListasF.this.context, null).show();
						return;
					}
					else if ( listas.exist(value)) {
						UI.showAlertDialog(getString(R.string.listas_upps),getString(R.string.listas_errror_nombre_repetido), getString(R.string.ok), ListasF.this.context, null).show();
					}

					((MyFragmentActivity)ListasF.this.context).trackEvents("Listas", "Cambiar Nombre","\""+ListasF.this.listas.getLista((int)info.id).getNombre()+"\" por \""+value+"\"", 0);
					ListasF.this.listas.getLista((int)info.id).setNombre(value);
					updateData();
					return;                  
				}  
			});  

			alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(input.getWindowToken(),0); 
					return;   
				}
			});
			alert.show();
			break;
		case R.id.email:
			StringBuilder emailBody = new StringBuilder(getString(R.string.body_mail));

			ArrayList<Product> products = this.listas.getLista((int)info.id).getProductos();
			if(products != null && products.size() > 0){
				for( Product product : products)
					emailBody.append(product.name + "\n");
			}

			Intent i = new Intent(Intent.ACTION_SEND);
			i.setType("text/plain");
			i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.listas_enviada));
			i.putExtra(Intent.EXTRA_TEXT   , emailBody.toString());
			try {
				((MyFragmentActivity)this.context).trackEvents("Listas", "Enviar por Correo", this.listas.getLista((int)info.id).getNombre(), 0);
				startActivity(Intent.createChooser(i, "Send mail..."));
			} catch (android.content.ActivityNotFoundException ex) {}

			break;
		case R.id.delete:
			AlertDialog.Builder delete = new AlertDialog.Builder(this.context);                 
			delete.setTitle(getString(R.string.confirmacion));  
			delete.setMessage(getString(R.string.eliminar));                

			delete.setPositiveButton(getString(R.string.si), new DialogInterface.OnClickListener() {  
				@Override
				public void onClick(DialogInterface dialog, int whichButton) {
					((MyFragmentActivity)ListasF.this.context).trackEvents("Listas", "Eliminar Lista", ListasF.this.listas.getLista((int)info.id).getNombre(), 0);
					ListasF.this.listas.dropList((int)info.id, ListasF.this.context);					
					updateData();
					return;                  
				}  
			});  

			delete.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					updateData();
					return;   
				}
			});
			delete.show();
			break;
		case R.id.add:
			((Arabela)this.context).setList(this.listas.getLista((int)info.id));
			Fragment fragmnet = new CategoriasF();
			((CategoriasF)fragmnet).setName("Categorias");
			((MyFragmentActivity)this.context).addDir("Categorias");
			((MyFragmentActivity)this.context).startNextFragment(fragmnet);
			break;
		default:
			return super.onContextItemSelected(item);
		}
		return true;
	}

	@Override
	public void updateData(){
		clear();
		loadDataFromFile();
		new ResGetLists(this.listas).run();
		this.l_adapter.notifyDataSetChanged();
	}

	private void addProduct(int position){
		if(this.product!=null){
			this.listas.getLista(position).addProduct(this.product, this.context);
			saveDataToFile();
			UI.showAlertDialog(getString(R.string.aviso), getString(R.string.producto_agregado),getString(R.string.ok), this.context, null).create().show();
			((Arabela) this.context).removeFragment();
		}
	}

	private  OnClickListener creaList = new OnClickListener(){
		@Override
		public void onClick(View arg0) {
			final AlertDialog.Builder alert = new AlertDialog.Builder(ListasF.this.context);                 
			alert.setTitle(getString(R.string.pedidos_nuevo));  
			alert.setMessage(getString(R.string.pedidos_nuevo_nombre));   
			final EditText input = new EditText(ListasF.this.context); 
			alert.setView(input);

			InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

			alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {  
				@Override
				public void onClick(DialogInterface dialog, int whichButton) {  
					String value = input.getText().toString();
					if (value.equals("")){
						UI.showAlertDialog(getString(R.string.listas_upps),getString(R.string.listas_error_nombre_vacio), getString(R.string.ok), ListasF.this.context, null).show();
						return;
					}
					creaLista(value);
					updateData();
					return;                  
				}  
			});  

			alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(input.getWindowToken(),0); 
					return;   
				}
			});
			alert.show();
		}
	};

	public void creaLista(String nombre){
		this.listas.createList(nombre, this.context);
		addProduct(0);
	}

	private void clear(){
		this.l_adapter.clear();
		this.l_adapter.notifyDataSetChanged();
	}

	private class ListsAdapter extends ArrayAdapter<Lista> {
		private int textViewResourceId; 

		public ListsAdapter(int textViewResourceId, ArrayList<Lista> listas ) {
			super(ListasF.this.context , textViewResourceId, listas);
			this.textViewResourceId = textViewResourceId;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View v = convertView;
			if (v == null) {
				LayoutInflater vi = LayoutInflater.from(ListasF.this.context);
				v = vi.inflate(this.textViewResourceId, null);
			}
			Lista o = super.getItem(position);
			if (o != null){				
				((TextView) v.findViewById(R.id.pedido_title)).setText(o.getNombre());
				((TextView) v.findViewById(R.id.pedido_fecha)).setText(getString(R.string.pedido_fecha)+" "+o.getDateOfCreation());
				((TextView) v.findViewById(R.id.pedido_amount)).setText(getString(R.string.pedido_total)+" "+Utils.stringtoCurrency(o.getTotalAmount().toString()));
				((TextView) v.findViewById(R.id.pedido_total)).setText("("+String.valueOf(o.getNumberProducts())+" "+getString(R.string.pedido_productos)+")");
				v.setBackgroundResource(R.drawable.selector_shape_fondo_list_item);
			}
			return v;
		}
	}

	class  ResGetLists implements Runnable {

		ArrayList <Lista> listas1;
		public ResGetLists (ListasPojo listas){
			if ( listas != null){
				this.listas1 = listas.getListas(); 
			} 
		}

		@Override
		public void run() {
			clear();
			if(this.listas1 != null && this.listas1.size() > 0){			
				for( Lista lista : this.listas1)
					ListasF.this.l_adapter.add(lista);
			}
			ListasF.this.l_adapter.notifyDataSetChanged();
		}
	}

	private BroadcastReceiver dataChanged = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			saveDataToFile();			
		}
	};
	private BroadcastReceiver update = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			updateData();	
			Intent i = new Intent(Recursos.UPDATE_PRODUCTS);
			i.putExtra("Listas", ListasF.this.listas);
			ListasF.this.context.sendBroadcast(i);
		}
	};
}
