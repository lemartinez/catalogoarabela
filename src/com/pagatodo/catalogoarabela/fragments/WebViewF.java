package com.pagatodo.catalogoarabela.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pagatodo.catalogoarabela.Arabela;
import com.pagatodo.catalogoarabela.R;

@SuppressLint({ "ValidFragment", "ValidFragment" })
public class WebViewF extends Fragment {

	private String url;
	private String name;
	private View view = null;
	private WebView webView = null;

	private Context context;


	public WebViewF(){

	}

	public WebViewF (String url,String name){
		this.url = url;
		this.name = name;
	} 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		if(savedInstanceState!=null){

			if (savedInstanceState.containsKey("url"))
				url = savedInstanceState.getString("url");

			if (savedInstanceState.containsKey("name"))
				name = savedInstanceState.getString("name");
		}

	}


	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		this.context = getActivity();
		this.view = inflater.inflate(R.layout.webview, container, false);
		this.webView= (WebView) this.view.findViewById(R.id.webview);
		this.webView.getSettings().setJavaScriptEnabled(true);
		this.webView.getSettings().setSupportZoom(true);
		//this.webView.getSettings().setSupportZoom(false);
//		this.webView.getSettings().setBuiltInZoomControls(true);
//		this.webView.getSettings().setDefaultZoom(ZoomDensity.FAR);
		this.webView.setWebViewClient(new InsideWebViewClient());
		return this.view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (this.url != null)
			this.webView.loadUrl(this.url);
	}



	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (url != null ) outState.putSerializable("url", url);
		if (name != null )  outState.putSerializable("name", name);
	}



	@Override
	public void onDestroy() {
		if(name!=null)
			((MyFragmentActivity)this.context).removeDir("/"+this.name+"/"+this.url);
		super.onDestroy();
		
		
	}

	private class InsideWebViewClient extends WebViewClient {
		private int       webViewPreviousState;
		private final int PAGE_STARTED    = 0x1;
		private final int PAGE_REDIRECTED = 0x2;
		ProgressDialog dialog;

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			this.webViewPreviousState = this.PAGE_REDIRECTED;
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			this.webViewPreviousState = this.PAGE_STARTED;
			try{
				if (this.dialog == null || !this.dialog.isShowing())
					this.dialog = ProgressDialog.show(WebViewF.this.context, "", getString(R.string.cargando), true, true,
							new OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							dialog.dismiss();
							((Arabela)WebViewF.this.context).removeFragment();
						}
					});
			}catch(Exception e){}
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			if (this.webViewPreviousState == this.PAGE_STARTED) {
				if (this.dialog != null){
					this.dialog.dismiss();
					this.dialog = null;
				}
			}
		}
	}
}
