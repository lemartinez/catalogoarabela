package com.pagatodo.catalogoarabela.fragments;

import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.pagatodo.catalogoarabela.AppArabela;
import com.pagatodo.catalogoarabela.Arabela;
import com.pagatodo.catalogoarabela.R;
import com.pagatodo.catalogoarabela.pojos.Categories;
import com.pagatodo.catalogoarabela.pojos.Categories2;
import com.pagatodo.catalogoarabela.pojos.Category;
import com.pagatodo.catalogoarabela.pojos.Category2;
import com.pagatodo.catalogoarabela.recursos.Recursos.AnsType;
import com.pagatodo.exceptions.OfflineException;
import com.pagatodo.interfaces.GetCategory;
import com.pagatodo.interfaces.Update;
import com.pagatodo.net.Net;
import com.pagatodo.ui.UI;
import com.pagatodo.utils.Sonido;
import com.pagatodo.utils.Utils;

public class CategoriasF  extends ListFragment implements Update, GetCategory{
	int mNum;
	private Sonido sonido;
	private final int  audio = R.raw.tab;
	private CategoriesAdapter t_adapter;
	private String id = null;
	private String name	= null;
	private boolean resume=false;
	private List<Category2> categories;
	private Activity context;
	private ProgressDialog m_ProgressDialog = null;

	private View viewer = null;
	private DisplayImageOptions options;
	private ImageLoader imageLoader = ImageLoader.getInstance();

	public CategoriasF(String id_category,List<Category2> categories) {
		this.categories = categories;
		this.id = id_category;
	}

	public CategoriasF(String id_category) {
		this.id = id_category;
	}

	public CategoriasF() {
		this(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		this.context =getActivity();
		sonido = new Sonido(this.context);
		sonido.initSonido(this.audio);
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.stub_)
		.cacheInMemory()
		.cacheOnDisc()
		.build();
		imageLoader.init(ImageLoaderConfiguration.createDefault(context));

		if(savedInstanceState!=null){
			
			if (savedInstanceState.containsKey("id"))
				id = savedInstanceState.getString("id");

			if (savedInstanceState.containsKey("categories"))
				categories = (List<Category2>) savedInstanceState.getSerializable("categories");
			
			if (savedInstanceState.containsKey("name"))
				name = savedInstanceState.getString("name");
			
		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (id != null ) outState.putSerializable("id", id);
		if (categories != null )  outState.putSerializable("categories", (Serializable) categories);
		if (name != null )  outState.putSerializable("name", name);
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		this.viewer = inflater.inflate(R.layout.categorias, container, false);

		if (getListAdapter() == null){
			this.t_adapter = new CategoriesAdapter(this.context, R.layout.category_item, new ArrayList<Category2>());
			setListAdapter(this.t_adapter);
		}
		updateData();
		return this.viewer;
	}

	@Override
	public void onResume() {
		super.onResume();
		this.resume =true;
	}

	@Override
	public void onPause() {
		super.onPause();
		this.resume=false;
	}


	@Override
	public void onDestroy() {
		
		if(this.name!=null)
			((MyFragmentActivity)this.context).removeDir("/"+this.name);
		imageLoader.stop();
		super.onDestroy();
	}


	private class CategoriesAdapter extends ArrayAdapter<Category2> {
		private int textViewResourceId; 
		public CategoriesAdapter(Context context, int textViewResourceId, ArrayList<Category2> categories ) {
			super(context , textViewResourceId, categories);
			this.textViewResourceId = textViewResourceId;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = LayoutInflater.from(CategoriasF.this.context);
				v = vi.inflate(this.textViewResourceId, null);
			}
			Category2 o = super.getItem(position);
			if (o != null){				
				((TextView) v.findViewById(R.id.cat_title)).setText(o.name);
				((TextView) v.findViewById(R.id.cat_number)).setText("(" + String.valueOf(o.products_total) + ")");

				if (o.image.equals("")== false){
					ImageView image=(ImageView)v.findViewById(R.id.product_image);
					image.setVisibility(View.VISIBLE);
					if(CategoriasF.this.resume==true){
						imageLoader.displayImage(o.image, image, options);
					}
				}
				else{
					ImageView image=(ImageView)v.findViewById(R.id.product_image);
					RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(0,LayoutParams.WRAP_CONTENT);
					image.setLayoutParams(params);
				}
				v.setBackgroundResource(R.drawable.selector_shape_fondo_list_item);
			}
			return v;
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Category2 o = (Category2) getListAdapter().getItem(position);
		if (o != null){
			clear();
			sonido.play(false);
			Fragment fragmnet = (o.final_ == false)   ? new CategoriasF(o.id,o.categories) : new ProductosF(o.id,o.products_total);
			if(o.final_==false){
				((CategoriasF)fragmnet).setName(o.name);
			}
			else{
				((ProductosF)fragmnet).setName(o.name);
			}
			( (MyFragmentActivity) this.context).addDir(o.name);
			( (MyFragmentActivity) this.context).startNextFragment(fragmnet);
		}
	}




	@Override
	public void updateData(){
		if(this.categories==null&&this.id==null){
			new Thread(null, this.runGetCategoriesTree, "getCategories").start();
			CategoriasF.this.m_ProgressDialog = ProgressDialog.show(getActivity(), getString(R.string.espere), getString(R.string.obteniendo), true,true,
					new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					( (Arabela) CategoriasF.this.context).removeFragment();
				}
			});
			clear();
		}else{
			if(this.categories==null&&this.id!=null){
				new Thread(null, this.runGetCategories, "getCategories").start();
				CategoriasF.this.m_ProgressDialog = ProgressDialog.show(getActivity(), getString(R.string.espere), getString(R.string.obteniendo), true,true,
						new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {
						( (Arabela) CategoriasF.this.context).removeFragment();
					}
				});
				clear();
			}else{
				AnsType ans = AnsType.OK;
				CategoriasF.this.context.runOnUiThread(new ResGetCategoriesTree(categories,ans));
			}			
		}
	}


	public void setName(String name){
		this.name=new String(name);
	}

	private void clear(){
		this.t_adapter.clear();
		this.t_adapter.notifyDataSetChanged();
	}



	/*
	 *Runnables para ejecutar los webServices getCategories y getProducts 
	 *
	 */


	private Runnable runGetCategoriesTree = new Runnable(){
		@Override
		public void run() {

			Categories2 categoriesTree=null;
			List <Category2> categories=null;
			boolean reintent=false;
			AnsType ansType = AnsType.UNKNOW;
			String  url = AppArabela.getInstance().getPrefs().loadData("URL_CATEGORIES");//Recursos.URL_CATEGORIES;
			try {
				categoriesTree=(Categories2) Net.sendDataAndGetObjectCache(url, Categories2.class, null);
				categories=categoriesTree.categories;
				CategoriasF.this.categories = categoriesTree.categories;
				ansType=AnsType.OK;
			} catch (HttpException e) {					
				reintent=Utils.getUrls();
				ansType = AnsType.NETWORK_PROBLEM;
			} catch (IOException e) {
				ansType = AnsType.NETWORK_PROBLEM;
				e.printStackTrace();
			} catch (OfflineException e) {
				ansType = AnsType.OFFLINE;
				e.printStackTrace();
			} catch (Exception e) {
				ansType = AnsType.UNKNOW;
			}
			if(reintent==true)
				new Thread(null, CategoriasF.this.runGetCategoriesTree, "getCategories").start();
			else
				CategoriasF.this.context.runOnUiThread(new ResGetCategoriesTree(categories,ansType));
		}
	};
	
	private Runnable runGetCategories= new Runnable(){
		@Override
		public void run() {
			Categories cat=null;
			List <Category2> categories=new ArrayList <Category2>();
			boolean reintent=false;
			AnsType ansType = AnsType.UNKNOW;
			try {
				String url = String.format( AppArabela.getInstance().getPrefs().loadData("URL_SERVER") + "categories/%s/",  URLEncoder.encode(id, "UTF-8")) ;
				cat=(Categories) Net.sendDataAndGetObjectCache(url, Categories.class, null);
				for( Category category : cat.categories){
					Category2 c=new Category2();
					c.categories=null;
					c.final_=category.final_;
					c.id=category.id;
					c.id_parent=null;
					c.image=category.image;
					c.name=category.name;
					c.products_total=category.products_total;
					c.total=0;
					categories.add(c);
				}
				CategoriasF.this.categories = categories;
				ansType=AnsType.OK;
			} catch (HttpException e) {					
				reintent=Utils.getUrls();
				ansType = AnsType.NETWORK_PROBLEM;
			} catch (IOException e) {
				ansType = AnsType.NETWORK_PROBLEM;
				e.printStackTrace();
			} catch (OfflineException e) {
				ansType = AnsType.OFFLINE;
				e.printStackTrace();
			} catch (Exception e) {
				ansType = AnsType.UNKNOW;
			}
			if(reintent==true)
				new Thread(null, CategoriasF.this.runGetCategoriesTree, "getCategories").start();
			else
				CategoriasF.this.context.runOnUiThread(new ResGetCategoriesTree(categories,ansType));
		}
	};

	class  ResGetCategoriesTree implements Runnable {

		List <Category2> categoriesTree;
		AnsType ansType;
		public ResGetCategoriesTree (List<Category2> categories,AnsType ans){
			if ( categories != null)
				this.categoriesTree = categories; 	
			if (ans!=null)
				this.ansType=ans;
			else
				this.ansType=AnsType.UNKNOW;

		}
		@Override
		public void run() {
			if(CategoriasF.this.m_ProgressDialog!=null)
				CategoriasF.this.m_ProgressDialog.dismiss();
			switch (this.ansType) {
			case OK:
				clear();
				if(this.categoriesTree != null && this.categoriesTree.size() > 0){			
					for( Category2 category : this.categoriesTree)
						CategoriasF.this.t_adapter.add(category);
				}
				CategoriasF.this.t_adapter.notifyDataSetChanged();
				break;
			case OFFLINE:
				UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_sin_internet), getString(R.string.ok), CategoriasF.this.context, null).create().show();
				break;
			case NETWORK_PROBLEM:
				UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_error), getString(R.string.ok), CategoriasF.this.context, null).create().show();
				break;
			case UNKNOW:
			case DATA_PROBLEM:
			default:
				UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_datos_corruptos), getString(R.string.ok), CategoriasF.this.context, null).create().show();
				break;
			}
		}
	}

	@Override
	public String getCategory() {
		return  this.id;
	}
}
