package com.pagatodo.catalogoarabela.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils.InsertHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.pagatodo.catalogoarabela.pojos.Product;
import com.pagatodo.catalogoarabela.pojos.ProductCategory;
import com.pagatodo.catalogoarabela.pojos.ProductDB;
import com.pagatodo.catalogoarabela.pojos.Products;
import com.pagatodo.catalogoarabela.recursos.Recursos;
import com.pagatodo.utils.Serialize;

@SuppressWarnings("deprecation")
public class MyDB {
	public static String Lock = "Lock";
	private SQLiteDatabase db;
	private MyDBHelper dbhelper;

	public MyDB (Context context){
		this.dbhelper = new MyDBHelper(context, Recursos.DATABASE_NAME, null, Recursos.DATABASE_VERSION);
	}

	public void open (){
		synchronized(Lock){
			try {
				this.db =this.dbhelper.getWritableDatabase();
			} catch (Exception e) {
				this.db =this.dbhelper.getReadableDatabase();
			}
		}
	}

	public void close(){
		synchronized(Lock){
			this.db.close();
			SQLiteDatabase.releaseMemory();
		}
	}

	public boolean isNull(){
		synchronized(Lock){
			return this.db==null? true:false;
		}
	}
	public void clearTables(){
		synchronized(Lock){
			String deleteProducts = "DELETE FROM " + Recursos.TABLE_PRODUCTOS;
			db.execSQL(deleteProducts);
			String deleteRelations = "DELETE FROM " + Recursos.TABLE_PRODUCTOS_CATEGORIAS;
			db.execSQL(deleteRelations);
		}
	}
	public void insertProductsCategories(List<ProductCategory> productCategories){
		synchronized(Lock){
			db.beginTransaction();
			try {
				InsertHelper ih = new InsertHelper(db,Recursos.TABLE_PRODUCTOS_CATEGORIAS);
				final int col1 = ih.getColumnIndex(Recursos.ID_PRODUCTO);
		        final int col2 = ih.getColumnIndex(Recursos.ID_CATEGORIA);
		        
		        for(ProductCategory productCategory:productCategories) {
		        	ih.prepareForInsert();
		            ih.bind(col1, Integer.parseInt(productCategory.id_prod));
		            ih.bind(col2, productCategory.id_cat);
		            ih.execute();
		        }
		        productCategories=null;
		        db.setTransactionSuccessful();
		        Log.e("Relaciones", "OK");
			} catch (Exception e) {}
			finally{
				db.endTransaction();
				System.gc();
			}
		}
	}
	
	public void insertProducts(List<ProductDB> products){
		synchronized(Lock){
			db.beginTransaction();
			try {
				InsertHelper ih = new InsertHelper(db,Recursos.TABLE_PRODUCTOS);
				final int col1 = ih.getColumnIndex(Recursos.ID_PRODUCTO);
		        final int col2 = ih.getColumnIndex(Recursos.PRODUCTO);
		        
		        for(ProductDB product:products) {
		        	ih.prepareForInsert();
		            ih.bind(col1, Integer.parseInt(product.id));
		            ih.bind(col2, Serialize.toByteAray(product.product));
		            ih.execute();
		        }
		        products=null;
		        db.setTransactionSuccessful();
			} catch (Exception e) {}
			finally{
				db.endTransaction();
				System.gc();
			}
		}
	}
	
	public Products getProductos(String cat){
		
		synchronized(Lock){
			String query = "SELECT p.ID,p.ProductoBlob FROM tbl_Productos p INNER JOIN tbl_ProductosCategorias pc ON p.ID_Producto=pc.ID_Producto WHERE pc.ID_Categoria = '"+cat+"' ORDER BY p.ID";
			Cursor c = this.db.rawQuery(query,null);
			List <Product> productos = new ArrayList<Product>();
			if (c.moveToFirst()) {
				do {	
					productos.add((Product) Serialize.toObject(c.getBlob(c.getColumnIndex("ProductoBlob"))));
				} while (c.moveToNext());
			}
			if (c != null && !c.isClosed()) {
				c.close();
			}
			c=null;
			Products p = new Products();
			p.products=productos;
			p.total=productos.size();
			p.page=1;
			p.total_pages=1;
			System.gc();
			return p;
		}
	}
}
