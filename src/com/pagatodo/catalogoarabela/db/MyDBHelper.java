package com.pagatodo.catalogoarabela.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.pagatodo.catalogoarabela.recursos.Recursos;

public class MyDBHelper extends SQLiteOpenHelper{

	public static String Lock = "Lock";
	private  static final String CREATE_TABLE_PRODUCTOS = "create table " +
			Recursos.TABLE_PRODUCTOS + " ("+
			Recursos.ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " +
			Recursos.ID_PRODUCTO + "  INTEGER, " + 
			Recursos.PRODUCTO  + " BLOB);";
	private  static final String CREATE_TABLE_PRODUCTOS_CATEGORIAS = "create table " +
			Recursos.TABLE_PRODUCTOS_CATEGORIAS + " ("+
			Recursos.ID_PRODUCTOCATEGORIA + "  INTEGER PRIMARY KEY AUTOINCREMENT, " +
			Recursos.ID_PRODUCTO + "  INTEGER, " + 
			Recursos.ID_CATEGORIA  + " TEXT);";

	public MyDBHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {	
			db.execSQL(CREATE_TABLE_PRODUCTOS);
			db.execSQL(CREATE_TABLE_PRODUCTOS_CATEGORIAS);
		} catch (Exception e) {}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("drop table if exists " + Recursos.TABLE_PRODUCTOS_CATEGORIAS);
		db.execSQL("drop table if exists " + Recursos.TABLE_PRODUCTOS);
		onCreate(db);
	}
}
