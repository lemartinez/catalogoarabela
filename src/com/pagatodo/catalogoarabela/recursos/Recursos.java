package com.pagatodo.catalogoarabela.recursos;

public class Recursos {


	public final static Boolean DEBUG = false;

	public static final String URL_SWITCH_SERVER;
	public static final String URL_SERVER;
	
	public static final String URL_CATEGORIES;
	public static final String URL_PRODUCTS;
	public static final String URL_VALIDATION_CACHE_FLUSH;

	static{
		if (DEBUG){
			URL_SWITCH_SERVER = 		"http://cferreyra.webfactional.com/php/URLs/arabela_urls.json";
			URL_CATEGORIES =			"http://storagecategoriasmarti.blob.core.windows.net/arabela/categoriesTree.json";
			URL_PRODUCTS =				"http://storagecategoriasmarti.blob.core.windows.net/arabela/Products.json";
			URL_SERVER =				"http://ec2-67-202-25-214.compute-1.amazonaws.com/Service1.svc/";
			URL_VALIDATION_CACHE_FLUSH ="http://cferreyra.webfactional.com/php/verify_app_arabela.json";
		}
		else {
			URL_SWITCH_SERVER = 		"http://cferreyra.webfactional.com/php/URLs/arabela_urls.json";
			URL_CATEGORIES =			"http://storagecategoriasmarti.blob.core.windows.net/arabela/categoriesTree.json";
			URL_PRODUCTS =				"http://storagecategoriasmarti.blob.core.windows.net/arabela/Products.json";
			URL_SERVER = 				"http://ec2-67-202-25-214.compute-1.amazonaws.com/Service1.svc/";
			URL_VALIDATION_CACHE_FLUSH ="http://cferreyra.webfactional.com/php/verify_app_arabela.json";
		}
	}




	public static final String URL_SERVER_SHORTENER = "https://www.googleapis.com/urlshortener/v1/url";

	public static final String ANALYTICS_ID = "UA-35290904-1";

	public static final String SEARCH = "com.pagatodo.miempresa.SEARCH";
	public static final String LAST_LOGIN_YEAR = "LAST_LOGIN_YEAR";
	public static final String LAST_LOGIN_MONTH = "LAST_LOGIN_MONT";
	public static final String LAST_LOGIN_DAY = "LAST_LOGIN_DAY";
	public static final String DATA_CHANGED_LISTS = "com.pagatodo.miempresa.DATA_CHAGED_LISTS";
	public static final String UPDATE_DATA = "com.pagatodo.miempresa.UPDATE_DATA";
	public static final String UPDATE_PRODUCTS = "com.pagatodo.miempresa.UPDATE_PRODUCTS";
	public static final String HIDE_TOP = "com.pagatodo.miempresa.HIDE_TOP";
	public static final String TRACK_EVENT = "com.pagatodo.marti.TRACK_EVENT";
	public static final String TRACK_PAGE = "com.pagatodo.marti.TRACK_PAGE";
	public static enum AnsType {OK, OFFLINE, NETWORK_PROBLEM, DATA_PROBLEM, UNKNOW }
	public static final String PATTERN_NUMENTERO="0";
	public static final String FILE_LISTAS =  "lst";
	public static final String HIDE_KEYBOARD = "com.pagatodo.HIDE_KEYBOARD";
	public static final String CLEAR_STACK = "com.pagatodo.CLEAR_STACK";
	public static final String CLEAR_PROMOTIONS = "com.pagatodo.CLEAR_PROMOTIONS";
	public static final String UPDATE_PROMOTIONS = "com.pagatodo.UPDATE_PROMOTIONS";
	public static final String HIDE_SPLASH = "com.pagatodo.HIDE_SPLASH";
	public static final String DEFAULT_IMG = "http://storagecategoriasmarti.blob.core.windows.net/categorias/product_default.png";

	/*Constantes para Twitter*/

	public static final String CALLBACK_URL = "http://pagatodomobile.com";
	public static final String URL_REQUEST = "https://api.twitter.com/oauth/request_token";
	public static final String URL_ACCESS = "https://api.twitter.com/oauth/access_token";
	public static final String URL_AUTH = "https://api.twitter.com/oauth/authorize";
	public static final String CONSUMER_KEY = "8URQUIZagph17JtsTVgQ";
	public static final String CONSUMER_SECRET = "qS2ViNJ8LgcOqRqFmakGo51f9EoIHq8wVX0nOGtX6A";
	public static final String TOKEN = "TOKEN";
	public static final String SECRET_TOKEN = "SECRET_TOKEN";
	public static final String CUENTA_TWITTER="@DeportesMarti";//"#DeportesMart\u00ED";//Cambiar por cuenta de Mart�

	/*Constantes para la DB*/

	public static final String DATABASE_NAME = "iburguer";
	public static final int DATABASE_VERSION =  1;
	
	public static final String TABLE_PRODUCTOS = "tbl_Productos";
	public static final String TABLE_PRODUCTOS_CATEGORIAS = "tbl_ProductosCategorias";
	public static final String ID = "ID";
	public static final String ID_PRODUCTO = "ID_Producto";
	public static final String CLAVE_PRODUCTO = "Clave";
	public static final String PRODUCTO = "ProductoBlob";
	public static final String ID_CATEGORIA = "ID_Categoria";
	public static final String ID_PRODUCTOCATEGORIA = "ID_ProductoCategoria";
}
