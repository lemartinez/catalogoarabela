package com.pagatodo.catalogoarabela;

import java.util.HashMap;
import java.util.Stack;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.pagatodo.catalogoarabela.fragments.Enviados;
import com.pagatodo.catalogoarabela.fragments.MyFragmentActivity;
import com.pagatodo.catalogoarabela.fragments.NoEnviados;
import com.pagatodo.catalogoarabela.fragments.ProductosF;
import com.pagatodo.catalogoarabela.fragments.ProductosListaF;
import com.pagatodo.catalogoarabela.fragments.PromocionesF;
import com.pagatodo.catalogoarabela.fragments.Recibidos;
import com.pagatodo.catalogoarabela.pojos.Lista;
import com.pagatodo.catalogoarabela.recursos.Recursos;
import com.pagatodo.interfaces.GetCategory;
import com.pagatodo.utils.Utils;

public class Arabela extends MyFragmentActivity{

	private static final int fragment_id = R.id.fg_catalogo;
	private View vCatalogo;
	private ActionBar actionBar;
	private Button btnSearch;
	private String dirCatalogo = "Cat�logo",dirPedidos="Pedidos";
	private Lista l;
	private SearchManager mSearch;
	private FrameLayout fl;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setFragmentId(R.id.fg_catalogo);
		super.setDir(dirCatalogo);
		setContentView(R.layout.activity_arabela);

		actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayUseLogoEnabled(true);

		fl=(FrameLayout) findViewById(R.id.fl_white);
		vCatalogo = (View)findViewById(R.id.rl_catalogo);
		mTabHost = (TabHost)findViewById(android.R.id.tabhost);
		btnSearch = (Button) findViewById(R.id.btn_search_catalogo);

		this.btnSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showWhiteBackground();
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(btnSearch, InputMethodManager.SHOW_IMPLICIT);
				btnSearch.requestFocus();
				onSearchRequested();
			}
		});
		mTabHost.setup();
		Utils.setDefaultPreferences();

		if (savedInstanceState != null) {
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
			backStacks = (HashMap<String, Stack<String>>) savedInstanceState.getSerializable("stacks");
		}else{
			initStacks();
			Fragment frag = new PromocionesF();
			startNextFragment(frag);
		}
		setTabs();
		IntentFilter filter = new IntentFilter(Recursos.SEARCH);
		registerReceiver(this.searchQuery, filter);
	}
	
	@Override
	public void onResume(){
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
		unregisterReceiver(this.searchQuery);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("tab", mTabHost.getCurrentTabTag());
		outState.putSerializable("stacks", backStacks);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.activity_arabela, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.catalogo) {
			vCatalogo.setVisibility(View.VISIBLE);
			mTabHost.setVisibility(View.GONE);
			actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.fondo_top_categorias));
			dirPedidos=getDir().contains("Cat�logo")?dirPedidos:getDir();
			super.setDir(dirCatalogo);
		} else if (item.getItemId() == R.id.pedidos) {
			vCatalogo.setVisibility(View.GONE);
			mTabHost.setVisibility(View.VISIBLE);
			actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.fondo_top_pedidos));
			dirCatalogo=getDir().contains("Pedidos")?dirCatalogo:getDir();
			super.setDir(dirPedidos);
			sendBroadcast(new Intent(Recursos.UPDATE_DATA));
		}
		invalidateOptionsMenu();
		return true;
	}

	@Override
	public void startNextFragment(Fragment fragment){
		if(vCatalogo.getVisibility()==View.VISIBLE)
			super.startNextFragment(fragment);
		else
			super.startNextFragmentToTab(fragment);
	}

	public void removeFragment(){
		Stack<String> backStack;
		int containerId;
		if(vCatalogo.getVisibility()==View.VISIBLE){
			backStack = backStacks.get(StackType.CATALOGO.toString());
			containerId = fragment_id;
		}
		else{
			backStack = backStacks.get(mTabManager.mTabHost.getCurrentTabTag());
			containerId = R.id.realtabcontent;
		}
		super.removeFragmentFromStack(backStack);
		showCorrectFragment(backStack, containerId);
	}

	@Override
	public void onBackPressed()
	{
		Stack<String> backStack;
		int containerId;
		if(vCatalogo.getVisibility()==View.VISIBLE){
			backStack = backStacks.get(StackType.CATALOGO.toString());
			containerId = fragment_id;
		}
		else{
			backStack = backStacks.get(mTabManager.mTabHost.getCurrentTabTag());
			containerId = R.id.realtabcontent;
		}
		if (backStack.isEmpty()||backStack.size()==1)
			finish();
		else{
			removeFragmentFromStack(backStack);
			showCorrectFragment(backStack, containerId);
		}
	}

	private void setTabs() {
		mTabManager = new TabManager(this, mTabHost, R.id.realtabcontent);
		mTabManager.addTab(mTabHost.newTabSpec(StackType.NO_ENVIADOS.toString()).setIndicator(createTabView("No Enviados")),
				NoEnviados.class, null);
		mTabManager.addTab(mTabHost.newTabSpec(StackType.ENVIADOS.toString()).setIndicator(createTabView("Enviados")),
				Enviados.class, null);
		mTabManager.addTab(mTabHost.newTabSpec(StackType.RECIBIDOS.toString()).setIndicator(createTabView("Recibidos")),
				Recibidos.class, null);
	}

	private View createTabView(String text) {
		View view = LayoutInflater.from(this).inflate(R.layout.tab_indicator, null);
		TextView tv = (TextView) view.findViewById(R.id.title);
		tv.setText(text);
		return view;
	}

	private void initStacks() {
		backStacks = new HashMap<String, Stack<String>>();
		backStacks.put(StackType.CATALOGO.toString(), new Stack<String>());
		backStacks.put(StackType.ENVIADOS.toString(), new Stack<String>());
		backStacks.put(StackType.NO_ENVIADOS.toString(), new Stack<String>());
		backStacks.put(StackType.RECIBIDOS.toString(), new Stack<String>());
	}

	/*Listas */

	public boolean inPedidos() {
		return vCatalogo.getVisibility() == View.VISIBLE? false : true;
	}

	public void dataChanged (){
		Intent intent = new Intent(Recursos.DATA_CHANGED_LISTS);
		sendBroadcast(intent);
	}

	public void setList(Lista l){
		this.l=l;
	}
	public Lista getList(){
		return this.l;
	}

	public void removetoProductosListaF(){
		Stack<String> backStack = backStacks.get(StackType.NO_ENVIADOS.toString());

		Fragment fragment;
		do{
			String tag = backStack.peek();
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			fragment = getSupportFragmentManager().findFragmentByTag(tag);
			if (fragment instanceof NoEnviados || fragment instanceof ProductosListaF)
				break;
			ft.remove(fragment);
			ft.commitAllowingStateLoss();
			backStack.pop();
		}while(!(fragment instanceof NoEnviados));
		showCorrectFragment(backStack, R.id.realtabcontent);
	}

	/*Busqueda*/

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
					MySuggestionProvider.AUTHORITY, MySuggestionProvider.MODE);
			suggestions.saveRecentQuery(query, null);
			showWhiteBackground(); 
			searchProducts(query);
		}
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_SEARCH){
			showWhiteBackground();
		}
		return super.onKeyUp(keyCode, event);
	}

	public void hideBarSearch(){
		btnSearch.setVisibility(View.GONE);
	}
	public void ShowBarSearch(){
		btnSearch.setVisibility(View.VISIBLE);
	}
	public void hideWhiteBackground(){
		fl.setVisibility(View.GONE);
	}
	public void showWhiteBackground(){
		fl.setVisibility(View.VISIBLE);
	}

	private void searchProducts(String query) {
		Intent intent = new Intent(Recursos.SEARCH);
		intent.putExtra("querySearch", query);
		sendBroadcast(intent);
		hideWhiteBackground();
	}

	@Override
	public boolean onSearchRequested() {
		super.onSearchRequested();
		this.mSearch = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
		this.mSearch.setOnCancelListener(new SearchManager.OnCancelListener(){
			@Override
			public void onCancel() {
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(0, 0);
				hideWhiteBackground();
			}
		});
		return false;
	}

	private BroadcastReceiver searchQuery = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Fragment v = null;
			String category = null;
			try {
				v = getSupportFragmentManager().findFragmentById(fragment_id);
				category =  ((GetCategory)v).getCategory() ;
			}catch (Exception e) {
				return;
			}
			String querySearch = intent.getStringExtra("querySearch") ;
			trackEvents("Productos", "Busqueda", querySearch, 0);
			ProductosF fragment=new  ProductosF( category, querySearch );
			fragment.setName("Busqueda="+querySearch+"/"+querySearch);
			addDir("Busqueda="+querySearch+"/"+querySearch);
			startNextFragment(fragment); 
		}

	};


	/**
	 * This is a helper class that implements a generic mechanism for
	 * associating fragments with the tabs in a tab host.  It relies on a
	 * trick.  Normally a tab host has a simple API for supplying a View or
	 * Intent that each tab will show.  This is not sufficient for switching
	 * between fragments.  So instead we make the content part of the tab host
	 * 0dp high (it is not shown) and the TabManager supplies its own dummy
	 * view to show as the tab content.  It listens to changes in tabs, and takes
	 * care of switch to the correct fragment shown in a separate content area
	 * whenever the selected tab changes.
	 */
	public static class TabManager implements TabHost.OnTabChangeListener {
		private final FragmentActivity mActivity;
		public final TabHost mTabHost;
		private final int mContainerId;
		private final HashMap<String, TabInfo> mTabs = new HashMap<String, TabInfo>();
		TabInfo mLastTab;

		static final class TabInfo {
			private final String tag;
			private final Class<?> clss;
			private final Bundle args;
			private Fragment fragment;
			TabInfo(String _tag, Class<?> _class, Bundle _args) {
				tag = _tag;
				clss = _class;
				args = _args;
			}
		}

		static class DummyTabFactory implements TabHost.TabContentFactory {
			private final Context mContext;
			public DummyTabFactory(Context context) {
				mContext = context;
			}
			@Override
			public View createTabContent(String tag) {
				View v = new View(mContext);
				v.setMinimumWidth(0);
				v.setMinimumHeight(0);
				return v;
			}
		}

		public TabManager(FragmentActivity activity, TabHost tabHost, int containerId) {
			mActivity = activity;
			mTabHost = tabHost;
			mContainerId = containerId;
			mTabHost.setOnTabChangedListener(this);
		}

		public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
			tabSpec.setContent(new DummyTabFactory(mActivity));
			String tag = tabSpec.getTag();
			TabInfo info = new TabInfo(tag, clss, args);
			mTabs.put(tag, info);
			mTabHost.addTab(tabSpec);
		}

		@Override
		public void onTabChanged(String tabId) {
			TabInfo newTab = mTabs.get(tabId);
			if (newTab.fragment == null) {
				newTab.fragment = Fragment.instantiate(mActivity,newTab.clss.getName(), newTab.args);
				((Arabela)mActivity).startNextFragmentToTab(newTab.fragment);
			} else {
				Stack<String> backStack = backStacks.get(newTab.tag);
				((Arabela)mActivity).showCorrectFragment(backStack, mContainerId);
			}
		}

		public int getContainerId(){
			return mContainerId;
		}
	}

	public static class MyTabListener<T extends Fragment> implements	TabListener {
		private Fragment mFragment;
		private final Activity mActivity;
		private final String mTag;
		private final Class<T> mClass;


		public MyTabListener(Activity activity, String tag, Class<T> clz) {
			mActivity = activity;
			mTag = tag;
			mClass = clz;
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			if (mFragment == null) {
				mFragment = Fragment.instantiate(mActivity, mClass.getName());
				ft.add(android.R.id.content, mFragment, mTag);
			} else {
				ft.attach(mFragment);
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			if (mFragment != null) {
				ft.detach(mFragment);
			}
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {}
	}
}
