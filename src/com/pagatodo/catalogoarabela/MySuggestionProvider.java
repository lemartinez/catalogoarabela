package com.pagatodo.catalogoarabela;

import android.content.SearchRecentSuggestionsProvider;

@SuppressWarnings("javadoc")
public class MySuggestionProvider extends SearchRecentSuggestionsProvider {
	public final static String AUTHORITY = "com.pagatodo.catalogoarabela.MySuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public MySuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}