package com.pagatodo.catalogoarabela;

import android.app.Application;

import com.pagatodo.persistencia.ObjectMemoryCache;
import com.pagatodo.persistencia.Preferencias;

public class AppArabela extends Application {

	private static AppArabela m_singleton;	
	private Preferencias prefs;
	private ObjectMemoryCache strMemoryCache;
	
	@Override
	public final void onCreate()
	{
		super.onCreate();
		m_singleton = this;
		this.prefs=  new Preferencias(this);
        this.strMemoryCache = new ObjectMemoryCache();
	}
	
	public static AppArabela getInstance(){
		return m_singleton;
	}	
	
	public Preferencias getPrefs() {
		return this.prefs;
	}

	public ObjectMemoryCache getStringMemoryCache(){
		return this.strMemoryCache;
	}
}
