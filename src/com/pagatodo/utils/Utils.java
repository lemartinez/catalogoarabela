package com.pagatodo.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import android.util.Log;

import com.pagatodo.catalogoarabela.AppArabela;
import com.pagatodo.catalogoarabela.pojos.Connections;
import com.pagatodo.catalogoarabela.recursos.Recursos;
import com.pagatodo.exceptions.OfflineException;
import com.pagatodo.net.Net;
import com.pagatodo.persistencia.Preferencias;

public class Utils {

	private static Connections currentsUrls;

	public static void CopyStream(InputStream is, OutputStream os){
		final int buffer_size=1024;
		try
		{
			byte[] bytes=new byte[buffer_size];
			for(;;)
			{
				int count=is.read(bytes, 0, buffer_size);
				if(count==-1)
					break;
				os.write(bytes, 0, count);
			}
		}
		catch(Exception ex){}
	}

	public static String formatNumberToMoney(String number){
		return String.format(Locale.US, "$%.2f",Float.valueOf(number));
	}

	public static String formatNumberToMoney(Double number){	
		return formatNumberToMoney(number,"$0.00");
	}

	public static String formatNumberToMoney(Double number,String pattern){
		DecimalFormat f = (DecimalFormat) NumberFormat.getInstance();
		f.setDecimalSeparatorAlwaysShown(true);
		f.applyPattern(pattern);

		return f.format(number ) ;
	}

	public static String stringtoCurrency (String amount){

		String ans = "";
		try {
			NumberFormat usFormat = NumberFormat.getCurrencyInstance(Locale.US);
			ans =  usFormat.format(Double.valueOf(amount));	
		} catch (Exception e) {
			ans = "$0.00";
		}

		return ans;

	}


	private  final static Pattern rfc2822 = Pattern.compile(
			"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$" );

	public static boolean isValidEmailAddress(String email){
		if (rfc2822.matcher(email).matches())
			return false;
		else 
			return true;

	}

	public static boolean compareDatetoToday (int dia, int mes, int anio){
		boolean ans = false;
		String date = anio + "/" + (mes) + "/" + dia;
		if (dia == 0 && mes == 0 && anio == 0)
			return true;
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
		try {
			Date today = Calendar.getInstance().getTime();
			Date birthDay = df.parse(date);

			if (today.before(birthDay)){
				ans = true;
			}
		} catch (ParseException e) {
			ans =  false;
		}
		return ans;
	}

	private static String bytesToHex(byte[] b) {
		char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
				'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
		StringBuffer buf = new StringBuffer();
		for (int j=0; j<b.length; j++) {
			buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
			buf.append(hexDigit[b[j] & 0x0f]);
		}
		return buf.toString();
	}

	public static String getSHA1(String msg){
		String ans = null;

		try {
			MessageDigest md;
			md = MessageDigest.getInstance("SHA1");
			md.update(msg.getBytes());
			ans = Utils.bytesToHex( md.digest() );
		} catch (NoSuchAlgorithmException e) {
			ans = String.valueOf( msg.hashCode() );
		}

		return ans;
	} 
	public static boolean paro(char car){
		boolean eq=true;
		if(car==','||car==';'||car>64&&car<91||car>96&&car<123||car=='/')
			eq=false;
		return eq;
	}

	public static boolean isNumber(char car){
		boolean res=false;
		if(car>47&&car<58)
			res=true;
		return res;
	}

	public static String parsePhoneNumber(String tel){
		String res=null;
		int count=0,index=0;
		char[] numeros=new char[10];
		char[] aux=new char[10];
		while(count<10&&index<tel.length()&&paro(tel.charAt(index))){
			if(isNumber(tel.charAt(index))){
				if((count==0&&tel.charAt(index)=='0')||(count==0&&tel.charAt(index)=='1'&&index==1)){}
				else{
					numeros[count]=tel.charAt(index);
					count++;
				}
			}
			index++;
		}
		if(count<10){
			if(count>7){
				int j=0;
				if(count==8){
					aux[0]='5';
					aux[1]='5';
					j=2;
				}
				if(count==9){
					aux[0]='5';
					j=1;
				}
				for(int i=0;i<8;i++){
					aux[i+j]=numeros[i];
				}
				res=new String(String.copyValueOf(aux));
			}
		}
		else{
			res=new String(String.copyValueOf(numeros));
		}
		return res;
	}

	/*Asigna URLs y Variables de control a las preferencias de usuario en caso de no existir,
	 *tomando valores constantes de la clase recursos.
	 *Solo se invoca al inicio de la aplicaci�n*/
	public static void setDefaultPreferences(){
		Preferencias prefs = AppArabela.getInstance().getPrefs();
		if(prefs.loadData("INICIALIZED")==null){
			prefs.saveData("INICIALIZED", "TRUE");
			prefs.saveData("URL_VERIF", Recursos.URL_VALIDATION_CACHE_FLUSH);
			prefs.saveData("URL_SERVER", Recursos.URL_SERVER);
			prefs.saveData("URL_CATEGORIES", Recursos.URL_CATEGORIES);
			prefs.saveData("URL_PRODUCTS", Recursos.URL_PRODUCTS);
			prefs.saveData("URL_SHORTENER", Recursos.URL_SERVER_SHORTENER);
//			numberIntents=0;
			Log.e("TEST", "Inicializado");
		}
	}

	/*Obtiene las URLs de respaldo y en caso de ser diferentes de las que se estan usando se cambian 
	 *en las preferencias de usuario. Cuenta con control de errores en caso de fallar la URL de respaldo*/
	public static boolean getUrls(){
		Connections urls = null;
		if(currentsUrls==null)
			currentsUrls = getCurrentConnections();
		Preferencias prefs = AppArabela.getInstance().getPrefs();
		boolean reintent = false;
		try {
			urls = (Connections) Net.sendDataAndGetObject(Recursos.URL_SWITCH_SERVER, Connections.class,null);
			if(!urls.equals(currentsUrls)){
				prefs.saveData("URL_SERVER", urls.URL_SERVER);
				prefs.saveData("URL_VERIF", urls.URL_VERIF);
				prefs.saveData("URL_CATEGORIES", urls.URL_CATEGORIES);
				prefs.saveData("URL_PRODUCTS", urls.URL_PRODUCTS);
				prefs.saveData("URL_SHORTENER", urls.URL_SHORTENER);
				reintent = true;
				currentsUrls=null;
			}
		} catch (OfflineException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return reintent;
	}
	
	/*Devuelv� las URLs que se encuentren actualmente en las preferencias de usuario*/
	public static Connections getCurrentConnections(){
		Preferencias prefs = AppArabela.getInstance().getPrefs();
		Connections c = new Connections();
		c.URL_CATEGORIES = prefs.loadData("URL_CATEGORIES");
		c.URL_PRODUCTS = prefs.loadData("URL_PRODUCTS");
		c.URL_SERVER = prefs.loadData("URL_SERVER");
		c.URL_VERIF = prefs.loadData("URL_VERIF");
		c.URL_SHORTENER = prefs.loadData("URL_SHORTENER");
		return c;
	}
	
	/*Asigna las URLs que se usaran para comparar en el metodo "getUrls"*/
	public static void setCurrentsURL(Connections c){
		currentsUrls = c;
	}
}