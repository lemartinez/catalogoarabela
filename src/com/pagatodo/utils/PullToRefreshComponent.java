package com.pagatodo.utils;

import android.os.Handler;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.pagatodo.interfaces.NoRefreshListener;
import com.pagatodo.interfaces.RefreshListener;

public class PullToRefreshComponent {
	private ListView listView;
	private Handler uiThreadHandler;

	private RefreshListener onPullUpRefreshAction = new NoRefreshListener();

	private boolean mayPullUpToRefresh = true;

	public PullToRefreshComponent(
			ListView listView, Handler uiThreadHandler) {
		this.listView = listView;
		this.uiThreadHandler = uiThreadHandler;
		this.initialize();
	}

	private void initialize() {
		this.listView.setOnScrollListener(new OnScrollListener(){
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {}
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if(scrollState == 0){ 
					if (PullToRefreshComponent.this.isPullingUpToRefresh()) {
						PullToRefreshComponent.this.beginPullUpRefresh();
					}
				}
			}
		});
	}

	private void beginRefresh(final RefreshListener refreshAction) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				PullToRefreshComponent.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						PullToRefreshComponent.this.refreshFinished(refreshAction);
					}
				});
			}
		}).start();
	}

	public void beginPullUpRefresh() {
		this.beginRefresh(this.onPullUpRefreshAction);
	}

	/**************************************************************/
	// Listeners
	/**************************************************************/


	public void setOnPullUpRefreshAction(RefreshListener onRefreshAction) {
		this.enablePullUpToRefresh();
		this.onPullUpRefreshAction = onRefreshAction;
	}

	public void refreshFinished(final RefreshListener refreshAction) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				refreshAction.refreshFinished();
			}
		});

	}

	private void runOnUiThread(Runnable runnable) {
		this.uiThreadHandler.post(runnable);
	}


	/**************************************************************/
	// HANDLE PULLING
	/**************************************************************/

	public boolean isPullingUpToRefresh() {

		boolean lastvisible=this.isLastVisible();
		return this.mayPullUpToRefresh&& lastvisible;
	}

	private boolean isLastVisible() {
		int count=this.listView.getCount();
		if (count == 0) {
			return true;
		} else {
			int lastvisible=this.listView.getLastVisiblePosition();
			if (lastvisible + 1==count) {
				return true;
			} 
			else {
				return false;
			}
		}
	}

	/**************************************************************/
	// State Change
	/**************************************************************/

	public RefreshListener getOnLowerRefreshAction() {
		return this.onPullUpRefreshAction;
	}

	public void disablePullUpToRefresh() {
		this.mayPullUpToRefresh = false;
	}

	public void enablePullUpToRefresh() {
		this.mayPullUpToRefresh = true;
	}
}
