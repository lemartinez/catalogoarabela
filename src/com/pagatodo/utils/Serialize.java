package com.pagatodo.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serialize {

	public static byte[] toByteAray (Object o ){

		byte[] ans = null;

		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream() ;
			ObjectOutputStream out = new ObjectOutputStream(bos) ;
			out.writeObject(o);
			out.close();
			ans = bos.toByteArray();
		} catch (IOException e) {}
		return ans;
	}
	public static Object toObject (byte[] byteArray){
		Object ans = null;
		try {
			ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(byteArray));
			ans = in.readObject();
			in.close();
		} catch (Exception e) {}
		return ans;
	}
}
