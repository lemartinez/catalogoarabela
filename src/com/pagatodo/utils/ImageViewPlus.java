package com.pagatodo.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pagatodo.catalogoarabela.R;


@SuppressLint("DrawAllocation")
public class ImageViewPlus extends ImageView{

	Float scaleFactor;
	public ImageViewPlus(Context context) {
		super(context);
	}
	
	public ImageViewPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}
	
	public ImageViewPlus(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs);
	}
	
	private void init(Context ctx, AttributeSet attrs) {
		TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.ImageViewPlus);
		this.scaleFactor = a.getFloat(R.styleable.ImageViewPlus_scaleFactor, 1);
		a.recycle();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int height = (int) (this.getWidth()*scaleFactor);
		if (this.getHeight() > height)
			this.setLayoutParams(new LinearLayout.LayoutParams(this.getWidth(), height));
	}	
}
