package com.pagatodo.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.SparseIntArray;

public class Sonido {

	private Context context ;
	private SoundPool soundPool;
	private SparseIntArray soundPoolMap;
	int  sonido;

	public Sonido (Context context){
		this.context = context;	
	}
	public void initSonido(int sonido){
		this.soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
		this.soundPoolMap = new SparseIntArray();
		this.soundPoolMap.put(sonido, soundPool.load(context,sonido, 1));
		this.sonido = sonido;
	} 


	public void play(){
		play(true);
	}

	public void play(final boolean maxVolume){   	

		new Thread(null,new Runnable() {
			@Override
			public void run() {
				float volume = 0.1f;
				if (maxVolume == false ){
					AudioManager mAudioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
					float streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
					volume = streamVolume / mAudioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION);
				}
				soundPool.play(soundPoolMap.get(sonido), volume, volume, 1, 0, 1f);
			}
		}, "Sonido").start();
	}
}
