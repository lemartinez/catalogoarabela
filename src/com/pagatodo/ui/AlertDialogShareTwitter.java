package com.pagatodo.ui;

import static com.pagatodo.catalogoarabela.recursos.Recursos.CONSUMER_KEY;
import static com.pagatodo.catalogoarabela.recursos.Recursos.CONSUMER_SECRET;
import static com.pagatodo.catalogoarabela.recursos.Recursos.CUENTA_TWITTER;
import static com.pagatodo.catalogoarabela.recursos.Recursos.SECRET_TOKEN;
import static com.pagatodo.catalogoarabela.recursos.Recursos.TOKEN;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pagatodo.catalogoarabela.AppArabela;
import com.pagatodo.catalogoarabela.R;
import com.pagatodo.persistencia.Preferencias;

public class AlertDialogShareTwitter{

	private String url;
	private Dialog dialog;
	private Context context;
	private Preferencias prefs;
	private Twitter twitter;
	private EditText textTweet;
	private TextView restantes;
	private Button tweet;
	private int letras;
	public AlertDialogShareTwitter (String url, Context context ){
		this.url = url;
		this.context = context;
		this.dialog = new Dialog(context);
		this.prefs= AppArabela.getInstance().getPrefs();
	}

	public void build (){

		this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.dialog.setContentView(R.layout.share_twitter_dialog);
		this.dialog.setCancelable(true);
		this.dialog.setTitle("Compartir via Twitter");

		restantes = (TextView) this.dialog.findViewById(R.id.tv_letras_restantes);
		Button cancel = (Button) this.dialog.findViewById(R.id.btn_cancel_tweet);
		tweet = (Button) this.dialog.findViewById(R.id.btn_tweet);
		textTweet = (EditText) this.dialog.findViewById(R.id.et_tweet);

		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialogShareTwitter.this.dialog.cancel();
			}
		});

		tweet.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new Thread(null, AlertDialogShareTwitter.this.postTweet, "AuthTwitter").start();
				InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
				UI.showToast("Estado actualizado correctamente",AppArabela.getInstance());
				AlertDialogShareTwitter.this.dialog.cancel();
			}
		});

		textTweet.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void afterTextChanged(Editable s) {
				Log.i("Tweeter", "click");
				letras=140-textTweet.length();
				restantes.setText(Integer.toString(letras));
				if(letras==140)
					tweet.setEnabled(false);
				else
					tweet.setEnabled(true);
			}
		});

		textTweet.setText(CUENTA_TWITTER + " " + url + " ");
		letras=140-textTweet.length();
		restantes.setText(Integer.toString(letras));

		textTweet.setSelection(textTweet.getText().length());
	}


	public void buildShow (){
		this.build();
		this.dialog.show();
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
	}

	private Runnable postTweet = new Runnable() {
		@Override
		public void run() {
			twitter = new TwitterFactory().getInstance();  
			twitter.setOAuthConsumer(CONSUMER_KEY,CONSUMER_SECRET);  
			twitter.setOAuthAccessToken(new AccessToken(prefs.loadData(TOKEN), prefs.loadData(SECRET_TOKEN)));
			try {
				twitter.updateStatus(textTweet.getText().toString());
			} catch (TwitterException e) {
				e.printStackTrace();
				prefs.clearPreference(TOKEN);
				prefs.clearPreference(SECRET_TOKEN);
			}
		}
	};
}

