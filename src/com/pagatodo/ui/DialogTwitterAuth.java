package com.pagatodo.ui;

import static com.pagatodo.catalogoarabela.recursos.Recursos.CALLBACK_URL;
import static com.pagatodo.catalogoarabela.recursos.Recursos.CONSUMER_KEY;
import static com.pagatodo.catalogoarabela.recursos.Recursos.CONSUMER_SECRET;
import static com.pagatodo.catalogoarabela.recursos.Recursos.SECRET_TOKEN;
import static com.pagatodo.catalogoarabela.recursos.Recursos.TOKEN;
import static com.pagatodo.catalogoarabela.recursos.Recursos.URL_ACCESS;
import static com.pagatodo.catalogoarabela.recursos.Recursos.URL_AUTH;
import static com.pagatodo.catalogoarabela.recursos.Recursos.URL_REQUEST;
import oauth.signpost.basic.DefaultOAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.pagatodo.catalogoarabela.AppArabela;
import com.pagatodo.catalogoarabela.R;
import com.pagatodo.catalogoarabela.fragments.MyFragmentActivity;
import com.pagatodo.persistencia.Preferencias;

@SuppressLint("SetJavaScriptEnabled")
public class DialogTwitterAuth extends Dialog{
	private CommonsHttpOAuthConsumer consumer;
	private DefaultOAuthProvider provider;
	private WebView webView;
	private String oauthVerifier;
	private Preferencias prefs;
	private ProgressBar progressBar;
	private FrameLayout mContent;
	private ImageView mCrossImage;
	private Context context;
	static final FrameLayout.LayoutParams FILL =
	        new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
	                         ViewGroup.LayoutParams.MATCH_PARENT);
	
	public DialogTwitterAuth(Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.context=context;
    }
	
	@SuppressWarnings("deprecation")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.prefs= AppArabela.getInstance().getPrefs();
        mContent = new FrameLayout(getContext());
        createCrossImage();

        int crossWidth = mCrossImage.getDrawable().getIntrinsicWidth();
        setUpWebView(crossWidth / 2);

        mContent.addView(mCrossImage, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        addContentView(mContent, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));

    }
	private void createCrossImage() {
        mCrossImage = new ImageView(getContext());
        mCrossImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	DialogTwitterAuth.this.dismiss();
            }
        });
        Drawable crossDrawable = getContext().getResources().getDrawable(R.drawable.close);
        mCrossImage.setImageDrawable(crossDrawable);
        mCrossImage.setVisibility(View.VISIBLE);
    }
	private void setUpWebView(int margin) {
        RelativeLayout webViewContainer = new RelativeLayout(getContext());
        webView = new WebView(getContext());
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setLayoutParams(FILL);
        webView.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params;
		params = new RelativeLayout.LayoutParams(50 , 50);
		params.addRule(RelativeLayout.CENTER_IN_PARENT, webViewContainer.getId());
        progressBar=new ProgressBar(getContext());
        progressBar.setLayoutParams(params);
        progressBar.setVisibility(View.VISIBLE);
        webViewContainer.setLayoutParams(FILL);
        webViewContainer.setPadding(margin, margin, margin, margin);
        webViewContainer.addView(webView);
        webViewContainer.addView(progressBar);
        mContent.addView(webViewContainer);
        new Thread(null, this.authTwitter, "AuthTwitter").start();
    }
	
	private Runnable authTwitter = new Runnable() {
		@Override
		public void run() {
			try {
				String authUrl=null;
			    consumer = new CommonsHttpOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);  
				provider = new DefaultOAuthProvider(URL_REQUEST, URL_ACCESS, URL_AUTH);
				provider.setOAuth10a(true);
				authUrl = provider.retrieveRequestToken(consumer, CALLBACK_URL);
				Log.e("AUTH", authUrl);
				webView.setWebViewClient(new WebViewClient() {
				   	@Override
					public void onPageStarted(WebView view, String url, Bitmap favicon) {
				   		super.onPageStarted(view, url, favicon);
				   		if(url.startsWith(CALLBACK_URL)) {
			    			try {
			    				if(url.indexOf("oauth_token=") != -1) {
			    					oauthVerifier=url.substring(url.indexOf("&oauth_verifier=")+("&oauth_verifier=").length(), url.length());
			    					DialogTwitterAuth.super.hide();
			    				    new Thread(null, DialogTwitterAuth.this.saveTokens, "SaveToken").start();
			    				    ((MyFragmentActivity) DialogTwitterAuth.this.context).Tweet();
			    				} else if(url.indexOf("error=") != -1) {	
			    					prefs.clearPreference(TOKEN);
			    					prefs.clearPreference(SECRET_TOKEN);
			    					dismiss();
				    			}
				    		} catch(Exception ex) {
				    			prefs.clearPreference(TOKEN);
		    					prefs.clearPreference(SECRET_TOKEN);
		    					dismiss();
				    		}
				    	}
				   	}
					@Override
			    	public void onPageFinished(WebView view, String url) {
						progressBar.setVisibility(View.INVISIBLE);
						super.onPageFinished(view, url);
				    }
				});
				webView.loadUrl(authUrl);
			} catch(Exception ex) {
		    	dismiss();
		    }
		}
	};
	
	private Runnable saveTokens = new Runnable() {
		@Override
		public void run() {
			try {
				provider.retrieveAccessToken(consumer, oauthVerifier);  
				String token=consumer.getToken();
				String secret=consumer.getTokenSecret();	
				prefs.saveData(TOKEN, token);
				prefs.saveData(SECRET_TOKEN, secret);
			} catch(Exception ex) {
				prefs.clearPreference(TOKEN);
				prefs.clearPreference(SECRET_TOKEN);
		    }finally{
		    	dismiss();
		    }
		}
	};


}
